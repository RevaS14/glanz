<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_details', function (Blueprint $table) {
            $table->id();
            $table->integer('UserId');
            $table->integer('OrderId');
            $table->string('BillingName');
            $table->string('BillingEmail');
            $table->string('BillingMobile');
            $table->string('BillingCountry');
            $table->string('BillingState');
            $table->string('BillingCity');
            $table->string('BillingAddress1');
            $table->string('BillingAddress2')->nullable();
            $table->string('BillingPincode');
            $table->string('ShippingName');
            $table->string('ShippingEmail');
            $table->string('ShippingMobile');
            $table->string('ShippingCountry');
            $table->string('ShippingState');
            $table->string('ShippingCity');
            $table->string('ShippingAddress1');
            $table->string('ShippingAddress2')->nullable();
            $table->string('ShippingPincode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_details');
    }
}
