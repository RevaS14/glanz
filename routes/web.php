<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\SiteController@index');
Route::post('/quick-view', 'App\Http\Controllers\SiteController@quickView');
Route::post('/add-to-cart', 'App\Http\Controllers\SiteController@addToCart');
Route::post('/add-to-wishlist', 'App\Http\Controllers\SiteController@addToWishlist');
Route::post('/remove-from-cart', 'App\Http\Controllers\SiteController@remove');
Route::get('/wishlist-remove/{id}', 'App\Http\Controllers\SiteController@removeWishlist');
Route::get('/clear-cart', 'App\Http\Controllers\SiteController@clearCart');
Route::get('/product/{link}', 'App\Http\Controllers\SiteController@product');
Route::get('/category/{link}', 'App\Http\Controllers\SiteController@category');
Route::get('/404', 'App\Http\Controllers\SiteController@error');
Route::get('/about-us', 'App\Http\Controllers\SiteController@aboutus')->name('about');
Route::get('/contact-us', 'App\Http\Controllers\SiteController@contactus')->name('contact');
Route::get('/login', 'App\Http\Controllers\SiteController@login');
Route::post('/register', 'App\Http\Controllers\SiteController@register');
Route::post('/user-login', 'App\Http\Controllers\SiteController@userLogin');
Route::post('/review', 'App\Http\Controllers\SiteController@review');
Route::get('/cart', 'App\Http\Controllers\SiteController@cart');
Route::get('/wishlist', 'App\Http\Controllers\SiteController@wishlist');
Route::get('/render-cart', 'App\Http\Controllers\SiteController@renderCart');
Route::post('/update-cart', 'App\Http\Controllers\SiteController@updateCart');
Route::post('/get-states', 'App\Http\Controllers\SiteController@getStates');
Route::post('/get-cart-total', 'App\Http\Controllers\SiteController@getCartTotal');
Route::post('/get-cities', 'App\Http\Controllers\SiteController@getCities');
Route::post('/place-order', 'App\Http\Controllers\SiteController@placeOrder');
Route::post('/choose-transport', 'App\Http\Controllers\SiteController@chooseTransport');
Route::get('/checkout', 'App\Http\Controllers\SiteController@checkout');
Route::get('/success', 'App\Http\Controllers\SiteController@success');
Route::get('/my-account', 'App\Http\Controllers\SiteController@myAccount');
Route::get('/terms-and-conditions', 'App\Http\Controllers\SiteController@termsAndConditions');
Route::get('/privacy-policy', 'App\Http\Controllers\SiteController@privacyPolicy');
Route::get('/return-policy', 'App\Http\Controllers\SiteController@returnPolicy');
Route::post('/edit-account', 'App\Http\Controllers\SiteController@editAccount');
Route::post('/search', 'App\Http\Controllers\SiteController@search');
Route::post('/subscribe', 'App\Http\Controllers\SiteController@subscribe');

Route::post('/change-password', 'App\Http\Controllers\SiteController@changePassword');
Route::get('/view-order/{id}', 'App\Http\Controllers\SiteController@viewOrder');
Route::get('/print-invoice/{id}', 'App\Http\Controllers\SiteController@printInvoice');
Route::get('/logout', 'App\Http\Controllers\SiteController@logout');

//admin routes
Route::get('/admin', 'App\Http\Controllers\AdminController@index');
Route::post('/admin/login', 'App\Http\Controllers\AdminController@login');
Route::get('/admin/dashboard', 'App\Http\Controllers\AdminController@dashboard');
Route::get('/admin/categories', 'App\Http\Controllers\AdminController@categories');
Route::get('/admin/add-category', 'App\Http\Controllers\AdminController@addCategory');
Route::post('/admin/manage-category', 'App\Http\Controllers\AdminController@manageCategory');
Route::get('/admin/edit-category/{id}', 'App\Http\Controllers\AdminController@editCategory');
Route::post('/admin/delete', 'App\Http\Controllers\AdminController@delete');

Route::get('/admin/banners', 'App\Http\Controllers\AdminController@banners');
Route::get('/admin/add-banner', 'App\Http\Controllers\AdminController@addBanner');
Route::post('/admin/manage-banner', 'App\Http\Controllers\AdminController@manageBanner');
Route::get('/admin/edit-banner/{id}', 'App\Http\Controllers\AdminController@editBanner');

Route::get('/admin/products', 'App\Http\Controllers\AdminController@products');
Route::get('/admin/add-product', 'App\Http\Controllers\AdminController@addProduct');
Route::post('/admin/manage-product', 'App\Http\Controllers\AdminController@manageProduct');
Route::get('/admin/edit-product/{id}', 'App\Http\Controllers\AdminController@editProduct');

Route::get('/admin/locations', 'App\Http\Controllers\AdminController@locations');
Route::get('/admin/add-location', 'App\Http\Controllers\AdminController@addLocation');
Route::post('/admin/manage-location', 'App\Http\Controllers\AdminController@manageLocation');
Route::get('/admin/edit-location/{id}', 'App\Http\Controllers\AdminController@editLocation');

Route::get('/admin/add-order', 'App\Http\Controllers\AdminController@addOrder');
Route::post('/admin/manage-order', 'App\Http\Controllers\AdminController@manageOrder');
Route::get('/admin/edit-order/{id}', 'App\Http\Controllers\AdminController@editOrder');

Route::get('/admin/users', 'App\Http\Controllers\AdminController@users');
Route::get('/admin/orders', 'App\Http\Controllers\AdminController@orders');
Route::get('/admin/invoice', 'App\Http\Controllers\AdminController@invoice');
Route::get('/admin/all-invoices', 'App\Http\Controllers\AdminController@allInvoices');
Route::post('/admin/create-invoice', 'App\Http\Controllers\AdminController@createInvoice');
Route::post('/admin/update-order', 'App\Http\Controllers\AdminController@updateOrder');
Route::get('/admin/view-order/{id}', 'App\Http\Controllers\AdminController@viewOrder');
Route::get('/admin/print-invoice/{id}', 'App\Http\Controllers\AdminController@printInvoice');
Route::get('/admin/print-minvoice/{id}', 'App\Http\Controllers\AdminController@printMinvoice');
Route::get('/admin/complete-order/{id}', 'App\Http\Controllers\AdminController@completeOrder');
Route::get('/admin/view-user/{id}', 'App\Http\Controllers\AdminController@viewUser');

