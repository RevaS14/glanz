
@extends('layouts.admin')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">

      <div class="row align-items-center mb-30 justify-content-between">
    <div class="col-lg-6 col-sm-6">
        <h6 class="page-title">Users</h6>
    </div>
   
</div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif

        <!-- SELECT2 EXAMPLE -->

         
          <table id="example2" class="table table-hover">
                  <thead>
                  <tr>
                    <th>User Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Created On</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $row)
                  <tr>
                    <td> {{ $row->name }}</td>
                   
                    <td> {{ $row->mobile }}</td>
                    <td> {{ $row->email }}</td>
                    <td> {{ $row->address_1 }}</td>
                    <td> <span class="badge badge--<?php if($row->Status=='A') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($row->Status=='A') { echo 'Active'; } else { echo 'Deactive'; } ?></span>  </td>
                    <td> {{ date('d-m-Y',strtotime($row->created_at)) }}</td>
                    <td> 
                    <a class="icon-btn delete" data-id="{{ $row->id }}" data-table="User"> <i class="fas fa-trash"></i></a></td>
                  </tr>
                  @endforeach
                  </tbody>
          </table>
            
         
        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



