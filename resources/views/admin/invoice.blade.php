  
  @extends('layouts.admin')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
      <div class="container-fluid">
        @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif

        <!-- SELECT2 EXAMPLE -->

        <style>
    #invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    /* padding: 15px; */
    border: 1px solid #aaa;
}

.invoice header {
    padding: 10px 10px;
    /* margin-bottom: 20px; */
    color: #fff;
    background: #0c8322;
    border-bottom: 1px solid #0c8322
}

.invoice .company-details {
    text-align: right
}
.total {
    font-weight:bold;
    font-size:15px;
}
.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px;
    padding: 10px;
}

.border-top {
    border-top: 1px solid #fff !important;
}
.invoice .invoice-to {
    text-align: left
}


.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 1.5rem;
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #0c8322
}

.invoice main {
    padding-bottom: 17px;
}

.invoice main .thanks {
    /* margin-top: -100px; */
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #0c8322;
    padding:10px;
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
}

.invoice table td,.invoice table th ,.invoice table tfoot td {
    padding: 10px;
    background: #eee;
    border-bottom: 1px solid #fff
}
.white {
    background:#fff !important;
}
.invoice table td {
    border-bottom: 0
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}
.items-table tbody tr:not(:first-child) td {
    padding-top:0 !important;
}
.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #0c8322;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: center;
    /* font-size: 1.2em */
}

.invoice table .no {
    color: #fff;
    /* font-size: 1.2em; */
    background: #0c8322
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #0c8322;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}


.invoice table tfoot tr td {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    background: #0c8322;
    color: #fff;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    /* .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    } */

    /* .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    } */

    /* .invoice>div:last-child {
        page-break-before: always
    } */
}

</style>
  <div class="loading"></div>
  <!-- Begin Page Content -->
  <div class="container-fluid" style="padding-left: 0.5rem;
    padding-right: 0.5rem;">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Generate Invoice <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn float-right" href="{{ url('/admin/all-invoices') }}">Show All Invoices</a></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
 
  <div class="card-body">

    <div class="row">
  <div class="col-md-12">

    <div id="invoice">
       <form method="post" action="{{ url('/admin/create-invoice')}}">
       @csrf
<div class="invoice overflow-auto">
    <div style="min-width: 600px">
        <header>
            <div class="row">
                <div class="col">
                    <h2 class="name" style="margin-top: 56px;">
                        GLANZ SPECIAL TOOLS
                    </h2>
                </div>
                <div class="col company-details">
                <div>
                   
            </div>
                    <h2 class="name">
                       
                    <select style="display: inline-block;
    width: 41%;" class="form-control" name="invoice_for" required>
                        <option value="">Select</option>
                        <option value="O">Original</option>
                        <option value="D">Duplicate</option>
                        <option value="C">Extra Copy</option>
                    </select>
                        
                    </h2>
                    <h2 class="name">
                       
                            INVOICE : <input type="text" class="form-control" style="display: inline-block;
    width: 41%;" name="invoice_no" required>
                        
                    </h2>
                    <div>Date : <input type="text" class="form-control" value="<?php echo date('d-m-Y'); ?>" style="display: inline-block;
    width: 41%;margin-top:10px;" required name="invoice_date"></div>
                    
                </div>
            </div>
        </header>
        <main>
            <div class="row contacts">
                <div class="col invoice-to">

                    <textarea required class="form-control" rows="6" placeholder="Bill To" name="bill_to" ></textarea>
                </div>
                <div class="col invoice-to">
                   
                    <div class="form-group">
                        <input type="text" name="delivery_note" class="form-control" placeholder="Delivery Note" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="buyers_order_no" class="form-control" placeholder="Buyer's Order No" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="dispatch" class="form-control" placeholder="Dispatch Through" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="lr" class="form-control" placeholder="LR.No & Date" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="suppliers_ref" class="form-control" placeholder="Supplier's Ref" required>
                    </div>
                                    
                </div>
                <div class="col invoice-details">
                <div class="form-group">
                    <input type="text" name="mode" class="form-control" placeholder="Mode / Terms of Payment" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="date" class="form-control" placeholder="Date" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="destination" class="form-control" placeholder="Destination" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="transport" class="form-control" placeholder="Transport" required>
                    </div>
                    <div class="form-group">
                    <input type="text" name="others" class="form-control" placeholder="Other References">
                    </div>
                </div>
            </div>
            <table border="0" cellspacing="0" cellpadding="0" class=" items-table">
                <thead>
                    <tr>
                        <th style="width: 50px;">No</th>
                        <th class="text-left" style="width: 350px;">DESCRIPTION</th>
                        <th class="text-center" style="width: 120px;">HSN CODE</th>
                        <th class="text-center" style="width: 120px;">QTY</th>
                        <th class="text-center" style="width: 120px;">UNITS</th>
                        <th class="text-center" style="width: 120px;">RATE</th>
                        <th class="text-center" style="width: 120px;">DISCOUNT</th>
                        <th class="text-right" style="width: 120px;">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; $sub=0;?>
                    <tr>
                      <td class="no"style="width:50px">1</td>
                      <td style="width: 350px;"><textarea name="description[]" required class="form-control" placeholder="Description"></textarea></td>
                      <td style="width: 120px;" class="unit"><input type="text" name="hsn[]" required class="form-control hsn" placeholder="HSN CODE"></td>
                      <td ><input type="text" name="qty[]" required class="form-control qty" placeholder="Qty" value="1"></td>
                      <td>PCs</td>
                      <td><input type="text" name="rate[]" required class="form-control rate" placeholder="RATE" value=""></td>
                      <td><input type="text" name="discount[]" required class="form-control discount" placeholder="DISCOUNT" value=""></td>
                      <td class="total text-right" style="width:150px"><span class="subTotal">0.00</span></td>
                    </tr>
                
            </table>

            <table>
            <tr>
                    <td style="width: 48px;" class="no"></td>
                    <td style="width: 335px;"></td>
                    <td style="width: 110px;" class="unit"></td>
                    <td class="qty" style="width: 120px;"></td>
                    <td class="qty" style="width: 120px;"></td>
                    <td class="qty" style="width: 110px;"></td>
                    <td class="qty" style="width: 110px;"></td>
                    <td class="total" style="width: 143px;"><button type="button" name="add" id="clone" class="btn btn-warning">Add Item</button> </td>
                </tr>
               
                    
                </tbody>
                <tfoot>

                    <tr>
                    <td class="no"></td>
                    <td></td>
                    <td class="unit"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="total"></td>
                </tr>
                <tr>
                        
                    <td class="no"></td>
                        <td class="text-left"><input type="number" name="transport_charges" id="transport_charges" class="form-control" placeholder="Transport Charges"></td>
                        <td class="unit"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="total text-right" id="transport">0.00</td>
                    </tr>
                <tr>
                    <td class="no"></td>
                    <td class="text-left">Taxable Value</td>
                    <td class="unit"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="total text-right" id="total" >0.00 </td>
                    <input type="hidden" name="taxable_value" class="form-control" id="taxable_value">
                </tr>
                  
                    <tr>
                        
                        <td class="no"></td>
                        <td class="text-left"><input type="number" name="cgst" class="form-control" id="cgst" placeholder="Output CGST@ 9%"></td>
                        <td class="unit"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="total text-right" id="cgst_total">0.00 </td>
                        <input type="hidden" name="cgst_ip" class="form-control" id="cgst_ip">
                    </tr>
                    <tr>
                        
                        <td class="no"></td>
                        <td class="text-left"><input type="number" name="sgst" class="form-control" id="sgst" placeholder="Output SGST@ 9%"></td>
                        <td class="unit"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="total text-right" id="sgst_total">0.00 </td>
                        <input type="hidden" name="sgst_ip" class="form-control" id="sgst_ip">
                    </tr>
                    <tr>
                        
                        <td class="no"></td>
                        <td class="text-left"><input type="number" name="gst" class="form-control" id="gst" placeholder="Output GST@ 18%"></td>
                        <td class="unit"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="total text-right" id="gst_total">0.00 </td>
                        <input type="hidden" name="gst_ip" class="form-control" id="gst_ip">
                    </tr>
                    <tr>
                    <td class="no"></td>
                    <td class="text-left">Grand Total</td>
                    <td class="unit"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="qty"></td>
                    <td class="total text-right" id="grand_total">0.00 </td>
                    <input type="hidden" name="gtotal_ip" class="form-control" id="gtotal_ip">
                </tr>
                    <tr class="white">
                        <td colspan="2" class="border-top white" style="font-weight:bold; font-size:15px">Amount (In words) : <span id="words"></span></td>
                        <td colspan="4" class="border-top white"></td>
                        <td colspan="2" class="border-top white">For Glanz Special Tools</td>
                    </tr>
                    <tr>
                        <td colspan="2"  class="white"></td>
                        <td colspan="4"  class="white"></td>
                        <td colspan="2"  class="white"></td>
                    </tr>
                    <tr>
                        <td colspan="2"  class="white"></td>
                        <td colspan="4" class="white"></td>
                        <td colspan="2" class="white"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="white">Company's PAN: DZPPS5510C</td>
                        <td colspan="4" class="white"></td>
                        <td colspan="2" class="white"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="white"></td>
                        <td colspan="4" class="white"></td>
                        <td colspan="2" class="white">Authorised Signatory</td>
                    </tr>
                </tfoot>
       </table>
            <div class="notices">
                <div>NOTICE:</div>
                <div class="notice"></div>
            </div>
        </main>
        <footer>
            Invoice was created on a computer and is valid without the signature and seal.
        </footer>
    </div>
    <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
    <div>

    </div>
</div>
<input type="submit" class="btn btn-danger float-left mt-2 ml-2" id="clear" name="clear" value="Clear">
            <input type="submit" class="btn btn-success float-right mt-2" name="save" value="Generate Invoice">
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        </form>
</div>
<!--prepare-->

  </div>
    </div>

    
  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

        
        
        <!-- /.row -->
        
        <!-- /.row -->
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<script>
$(document).ready( function () {

    $('#datatable').DataTable({
        initComplete: function () {
            $('.loading').hide();
            $('#tabArea').show();
        }
    });

    $("body").on("click", "#clone", function(e){
      e.preventDefault();
        var clonedRow = $('.items-table tbody tr:first').clone();
        clonedRow.find('input').val('');
        var sno = $('.items-table tbody tr').length;
        sno = parseInt(sno);
        clonedRow.find('.no').text(sno+1);
        clonedRow.find('textarea').val('');
        clonedRow.find('input[name="rate[]"]').val('');
        clonedRow.find('input[name="qty[]"]').val(1);
        clonedRow.find('.subTotal').html('0.00');
        $('.items-table tbody').append(clonedRow);
        numberRows();
    });

    $('.items-table tbody').on('keyup', 'input[name="qty[]"]', function (e){
        if($(this).val()!='') {
              this.value = this.value.replace(/[^0-9\.]/g,'');
              var qty = this.value
              var rate = parseFloat($(this).closest('tr').find('td input[name="rate[]"]').val());
              var discount = parseFloat($(this).closest('tr').find('td input[name="discount[]"]').val());

              var total = parseFloat(rate*qty);
              if(discount) {
                total = total - ( (total/100) * discount );
              }
              $(this).closest('tr').find('td span.subTotal').html(total.toFixed(2));
              calculateTotal();
        }
  });

  $('.items-table tbody').on('keyup', 'input[name="rate[]"]', function (e){
        if($(this).val()!='') {
              this.value = this.value.replace(/[^0-9\.]/g,'');
              var rate = this.value
              var qty = parseFloat($(this).closest('tr').find('td input[name="qty[]"]').val());
              var discount = parseFloat($(this).closest('tr').find('td input[name="discount[]"]').val());
              if(!qty) {
                alert('Please enter quantity');
                return false;
              }
              var total = parseFloat(rate*qty);
              if(discount) {
                total = total - ( (total/100) * discount );
              }
              $(this).closest('tr').find('td span.subTotal').html(total.toFixed(2));
              calculateTotal();
        }
  });

  $('.items-table tbody').on('keyup', 'input[name="discount[]"]', function (e){
        if($(this).val()!='') {
              this.value = this.value.replace(/[^0-9\.]/g,'');
              var rate = parseFloat($(this).closest('tr').find('td input[name="rate[]"]').val());
              var qty = parseFloat($(this).closest('tr').find('td input[name="qty[]"]').val());
              var discount = this.value;

              var total = parseFloat(rate*qty);
              if(discount) {
                total = total - ( (total/100) * discount );
              }
              $(this).closest('tr').find('td span.subTotal').html(total.toFixed(2));
              calculateTotal();
        }
  });

  $('body').on('click', '#clear', function (e){
    e.preventDefault();
    location.reload();
  });

  function numberRows() {
      $('.sno').each(function(i) {
            $(this).html(i + 1);
      });
  }

  $('body').on('keyup', '#transport_charges, #cgst, #sgst, #gst', function (e){
    calculateTotal();
  });

  function calculateTotal() {
      net = 0;
    $('span.subTotal').each(function( index ) {
      net += parseFloat($( this ).html());
    });
   

    qty = 0;
    $('input[name="qty[]"]').each(function( index ) {
      qty += parseFloat($( this ).val());
    });

    
    var total = parseFloat(net.toFixed(2));
    var transport_charges = parseFloat($('#transport_charges').val());
    var cgst = parseFloat($('#cgst').val());
    var sgst = parseFloat($('#sgst').val());
    var gst = parseFloat($('#gst').val());
   
    if(isNaN(transport_charges)) { 
        transport_charges = 0;
    }
    total = total+transport_charges;
    $('#total').html(total.toFixed(2));
    $('#taxable_value').val(total.toFixed(2));
    if(isNaN(cgst)) { 
        cgst = 0;
    } else {
        cgst = (total/100) * cgst ;
    }
    if(isNaN(sgst)) { 
        sgst = 0;
    } else {
        sgst = (total/100) * sgst ;
    }
    if(isNaN(gst)) { 
        gst = 0;
    } else {
        gst = (total/100) * gst ;
    }
    var gtotal = parseFloat(total+cgst+sgst+gst);
    $('#transport').html(transport_charges.toFixed(2));
    $('#cgst_total').html(cgst.toFixed(2));
    $('#cgst_ip').val(cgst.toFixed(2));
    $('#sgst_total').html(sgst.toFixed(2));
    $('#sgst_ip').val(sgst.toFixed(2));
    $('#gst_total').html(gst.toFixed(2));
    $('#gst_ip').val(gst.toFixed(2));
    $('#grand_total').html(Math.ceil(gtotal).toFixed(2));
    $('#gtotal_ip').val(Math.ceil(gtotal).toFixed(2));
    
    if(gtotal>0) {
        var word = numberToWords(Math.ceil(gtotal));
        $('#words').html(word);
    // $.ajax({
    //     type: 'POST',
    //     url: base_url+"invoice/number-to-words/",
    //     data: {amount : gtotal},
    //     dataType: "text",
    //     success: function(resultData) { 
    //         $('#words').html(resultData);
    //         }
    // });
  }
  }

  function numberToWords(amount) {  
    
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string + ' Rupees Only.';
} 
 

} );

</script>
@stop