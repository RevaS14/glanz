
@extends('layouts.default')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <div class="row align-items-center mb-30 justify-content-between">
        <div class="col-lg-6 col-sm-6">
            <h6 class="page-title">Add Location</h6>
        </div>
        <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/locations') }}">Go Back</a>
        </div>
      </div>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
 
          <!-- /.card-header -->
          <div class="card-body">
          <div class="col-md-8">
            <form class="form-horizontal" action="{{ url('/manage-location')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="<?php if($data) { echo $data->id; } ?>">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control form-control-border" name="name" id="inputEmail3" required placeholder="Name" value="<?php if($data) { echo $data->name; } ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Pincode</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control form-control-border" name="pincode" id="inputEmail3" required placeholder="Pincode" value="<?php if($data) { echo $data->pincode; } ?>">
                    </div>
                  </div>
                 
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-7">
                    <select class="custom-select form-control-border" name="status" id="exampleSelectBorder">
                    <option value="A" <?php if($data) { if($data->status=='A') { echo 'selected'; }} ?>>Active</option>
                    <option value="D" <?php if($data) { if($data->status=='D') { echo 'selected'; }} ?>>Deactive</option>
                  </select>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <div class="">
                      <button type="submit" class="btn btn-sm btn--primary box--shadow1">Submit</button>   
                      </div>
                    </div>
                  </div>
                <!-- /.card-body -->
               
              </form>
              </div>

            
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
           
          </div>
        </div>
        <!-- /.card -->

        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



