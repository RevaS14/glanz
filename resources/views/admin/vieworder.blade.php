
@extends('layouts.admin')
@section('content')
  
<div class="content-wrapper" style="min-height: 1602px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order Details</h1>
          </div>
          <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/admin/orders') }}">Go Back</a>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
      @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">

            <div class="row">
                <div class="col-12">
                  <h4>
                    Order No: {{ $order->OrderNo }} 
                    <a data-toggle="modal" data-target="#exampleModal"><span class="badge badge--<?php if($order->Status=='D') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($order->Status=='D') { echo 'Delivered'; } elseif($order->Status=='PR') { echo 'Processing'; } elseif($order->Status=='P') { echo 'Pending'; }  elseif($order->Status=='ID') { echo 'Item Dispatched'; } ?></span> </a>
                    <small class="float-right">Date: {{ date('d-m-Y',strtotime($order->created_at)) }} </small>
                    <?php if($order->DeliveryDate) { ?> 
                      <br>
                    <small class="float-right">Delivery Date: {{ date('d-m-Y',strtotime($order->DeliveryDate)) }} </small>
                    <?php } ?>
                  </h4>
                 
                </div>
                <!-- /.col -->
              </div>
              
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  Billing Address
                  <address>
                    <strong>{{ $customer->name }}</strong><br>
                    {{ $customer->address_1 }}<br>
                    <?php if($customer->address_2) { 
                    echo $customer->address_2; echo '<br>';
                     } ?>
                    Phone: {{ $customer->mobile }}<br>
                    Email: {{ $customer->email }}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  Shipping Address 
                  <address>
                    <strong>{{ $shipping->BillingName }} </strong><br>
                    {{ $shipping->BillingAddress1 }}<br>
                    <?php if($shipping->BillingAddress1) { 
                    echo $shipping->BillingAddress1; echo '<br>';
                     } ?>
                    Phone: {{ $shipping->BillingMobile }}<br>
                    Email: {{ $shipping->BillingEmail }}
                  </address>
                </div>
                <div class="col-sm-4 invoice-col">
                <?php if($order->LR) { ?> 
                  <h6>LR Receipt</h6>
                    <img width="300" src="{{ asset($order->LR) }}">

                    <?php } ?>
                </div>
                <!-- /.col -->
               
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                <table class="table order">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Qty</th>
                                                <th>Discount</th>
                                                <th>Weight</th>
                                                <th>Price</th>
                                                
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $row)
                                            <tr>
                                                <td> <?php $image = $controller->getProductSingleImage($row->ProductId); $pro = $controller->getSingleProduct($row->ProductId); ?>
                                                    <a href="{{ url('/product/'.$row->ProductLink) }}"><img width="50" src="{{ url($image) }}" alt=""></a>
                                                </td>
                                                <td>{{ $row->ProductName }} </td>
                                                <td>{{ $row->Qty }} </td>   
                                                <td>{{ $pro->Discount }}% </td>
                                                <td>{{ $controller->readableMetric($row->Qty * $pro->Weight) }}</td>
                                                <td>Rs. {{ $row->Price }} </td>
                                                
                                                <td style="text-align:right">{{ $row->Total }} </td>                                               
                                            </tr>
                                            @endforeach

                                            
                                        </tbody>
                                        <tfoot id="carttotal">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Subtotal</th>
                                            <td style="text-align:right">{{ $order->Total }}</td>
                                        </tr>
                                        <div >
                                        <?php $tax = (18 / 100) * $order->Total; if($customer->State != 35 && $customer->State != '') { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>IGST (18%)</th>
                                            <td style="text-align:right"> <?php echo $tax; ?></td>
                                        </tr>
                                        <?php } else { ?>
                                            <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>CGST (9%)</th>
                                            <td style="text-align:right"> <?php echo (9 / 100) * $order->Total; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>SGST (9%)</th>
                                            <td style="text-align:right"> <?php echo (9 / 100) * $order->Total; ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if($order->CourierCharges) { ?> 
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Courier Charges</th>
                                            <td style="text-align:right">
                                                <?php echo $order->CourierCharges; ?>
                                            </td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Packing & Transport Charges</th>
                                            <td style="text-align:right">
                                                <?php echo $order->PackingTransportCharges; ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        
                                        <tr class="order_total">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Order Total</th>
                                            <td style="text-align:right"><strong>Rs. <?php $round = round($order->GrandTotal); ?> {{ $round }}</strong></td>
                                        </tr>
                                        </div>
                                    </tfoot>
                                    </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                 
                  <a target="_blank" href="{{ url('/print-invoice/'.$order->id) }}" class="btn btn-primary float-left" style="margin-right: 5px;">
                    <i class="fas fa-print"></i> Print
                  </a>
                  <?php if($order->Status=='P') { ?>
                  <a href="{{ url('/admin/complete-order/'.$customer->email, $order->id) }}" class="btn btn-success float-right" style="margin-right: 5px;">
                    <i class="far fa-credit-card"></i> Mark as Completed
                  </a>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{ url('/admin/update-order') }}" enctype="multipart/form-data">
      @csrf 
      <input type="hidden" name="orderId" value="{{ $order->id }}">
      <div class="form-group row">
        <label for="inputPassword" class="col-sm-4 col-form-label">Change Status</label>
        <div class="col-sm-8">
            <select class="form-control" id="status" name="status" <?php if($order->Status=='D') { echo 'disabled'; } ?>>
              <option value="PR" <?php if($order->Status=='PR') { echo 'selected'; } ?>>Processing</option>
              <option value="ID" <?php if($order->Status=='ID') { echo 'selected'; } ?>>Item Dispatched</option>
              <option value="D" <?php if($order->Status=='D') { echo 'selected'; } ?>>Delivered</option>
            </select>
        </div>
      </div>
      <div id="deliverDiv" <?php if($order->Status!='ID') { echo 'style="display:none"'; } ?>>
      <div class="form-group row">
        <label for="inputPassword" class="col-sm-4 col-form-label">LR Receipt</label>
        <div class="col-sm-8">
          
          <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
          <?php if($order->LR) { ?> 
            <br>
            <img style="width:100%" src="{{ asset($order->LR) }}">
          <?php } ?>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword" class="col-sm-4 col-form-label">Expected Delivery Date</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="deliveryDate" id="deliveryDate" value="<?php if($order->DeliveryDate) { echo date('d-m-Y',strtotime($order->DeliveryDate)); } ?>">
        </div>
      </div>
      </div>
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?php if($order->Status!='D') { ?> 
        <button type="submit" name="submit" class="btn btn-primary">Save</button>
        <?php }  ?>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
  $(function(){
    $("#status").change(function(e) {
        val = $(this).val();
        //alert(val);
        if(val=='ID') {
            $('#deliverDiv').show();
            $("#deliveryDate").prop('required',true);
        } else {
            $('#deliverDiv').hide();
            $("#deliveryDate").prop('required',false);
        }
    });
  });
</script>
  @stop



