
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Print invoice</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/dist/css/adminlte.min.css') }}">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h2 class="page-header">
         <img src="{{ url('public/assets/images/logo.png')}}" width="150">
          <small class="float-right">Order No: {{ $order->OrderNo }}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        From
        <address>
            <strong>{{ $customer->UserName }}</strong><br>
            {{ $customer->Address }}<br>
            
            Phone: {{ $customer->Mobile }}<br>
            Email: {{ $customer->Email }}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
        <strong>{{ $shipping->Name }} </strong><br>
        {{ $shipping->Address }}<br>
        Phone: {{ $shipping->Mobile }}<br>
        Email: {{ $shipping->Email }}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
          <b>Date: {{ date('d-m-Y',strtotime($order->created_at)) }}</b>
        <br>
        <b>Status:  <?php if($order->Status=='C') { echo 'Delivered'; } else { echo 'Pending'; } ?>
        </b>
       
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
          <th>Product Name</th>
        <th>Image</th>
        <th>Weight</th>
        <th>Qty</th>
        <th>Price</th>
        <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
          <?php $total =0 ; ?>
            @foreach($orders as $row)
            <tr>
                <td>{{ $row->ProductName }}</td>
                <td> <img width="100" src="{{ asset($row->ImagePath) }}"> </td>
                <td>{{ $row->Weight }}</td>
                <td>{{ $row->Qty }}</td>
                
                <td>Rs. {{ $row->Price }}</td>
                <td>Rs. {{ $row->Qty*$row->Price }}</td>
                <?php $total = $total + $row->Qty*$row->Price; ?>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-8">
       
      </div>
      <!-- /.col -->
      <div class="col-4">

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>Rs. {{ $total }}</td>
            </tr>
            <tr>
              <th>Tax (9.3%)</th>
              <td>Rs. 20</td>
            </tr>
            <tr>
              <th>Delivery Charge:</th>
              <td>Rs. 50</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>Rs. {{ $total+20+50 }}</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
<!-- Page specific script -->
<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
