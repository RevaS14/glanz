
@extends('layouts.admin')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <div class="row align-items-center mb-30 justify-content-between">
        <div class="col-lg-6 col-sm-6">
            <h6 class="page-title">Add Product</h6>
        </div>
        <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/admin/products') }}">Go Back</a>
        </div>
      </div>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
 
          <!-- /.card-header -->
          <div class="card-body">
          <div class="col-md-8">
            <form class="form-horizontal" action="{{ url('/admin/manage-product')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="productId" value="<?php if($data) { echo $data->id; } ?>">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Product Name</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control form-control-border" name="productName" id="inputEmail3" required placeholder="Product Name" value="<?php if($data) { echo $data->ProductName; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                      <textarea id="summernote" class="form-control form-control-border" name="description" required placeholder="Description"> <?php if($data) { echo $data->Description; } ?></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-7">
                   
                    @foreach($categories as $row)
                    <?php if($data) { $selected = $controller->getSelected($data->id,$row->id); } ?>
                    <input type="checkbox" name="category[]" id="<?php echo $row->id; ?>" <?php if($data) { if($selected) { echo 'checked'; } }?> value="<?php echo $row->id; ?>">
                    <label for="<?php echo $row->id; ?>"><?php echo $row->CategoryName; ?></label>
                    <br>
                    @endforeach
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Price</label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control form-control-border" name="price" id="inputEmail3" required placeholder="Price" value="<?php if($data) { echo $data->Price; } ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Weight</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control form-control-border" name="weight" id="inputEmail3" required placeholder="Weight" value="<?php if($data) { echo $data->Weight; } ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-7">
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image[]" class="custom-file-input form-control-border" multiple <?php if(!$data) { echo 'required'; } ?> id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    
                    </div>                    
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Stock</label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control form-control-border" name="stock" id="inputEmail3" required placeholder="Stock" value="<?php if($data) { echo $data->Stock; } ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Discount</label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control form-control-border" name="discount" id="inputEmail3" required placeholder="Discount" value="<?php if($data) { echo $data->Discount; } ?>">
                    </div>
                  </div>
                  @if($data && $images) 
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Existing Image</label>
                    <div class="col-sm-7">
                  @foreach($images as $row)
                    <div class="images" style="display:inline-block">
                    <img width="100" style="border: 1px solid #000;" src="{{ asset($row->Image) }}">
                    <a class="icon-btn delete" style="position:relative; right: 25px;
    top: -31px;" data-id="{{ $row->id }}" data-table="ProductImage"> <i class="fas fa-trash"></i></a></td>
                   
                    </div>
                    
                  @endforeach
                  </div>
                  
                 
                  </div>
                  @endif

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Is Featured</label>
                    <div class="col-sm-7">
                      <select class="custom-select form-control-border" name="isFeatured" id="exampleSelectBorder">
                        <option value="Y" <?php if($data) { if($data->Status=='Y') { echo 'selected'; }} ?>>Yes</option>
                        <option value="N" <?php if($data) { if($data->Status=='N') { echo 'selected'; }} ?>>No</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-7">
                    <select class="custom-select form-control-border" name="status" id="exampleSelectBorder">
                    <option value="A" <?php if($data) { if($data->Status=='A') { echo 'selected'; }} ?>>Active</option>
                    <option value="D" <?php if($data) { if($data->Status=='D') { echo 'selected'; }} ?>>Deactive</option>
                  </select>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <div class="">
                      <button type="submit" class="btn btn-sm btn--primary box--shadow1">Submit</button>   
                      </div>
                    </div>
                  </div>
                <!-- /.card-body -->
               
              </form>
              </div>

            
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
           
          </div>
        </div>
        <!-- /.card -->

        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



