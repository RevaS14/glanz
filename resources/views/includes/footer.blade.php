<!--call to action start-->
<section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="call_action_inner">
                        <div class="call_text">
                            <h3>We Have <span>Recommendations</span> for You</h3>
                            <p>Get plenty of hight quality tools!</p>
                        </div>
                        <div class="discover_now">
                            <a href="#">Shop now</a>
                        </div>
                        <div class="link_follow">
                            <ul>
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                                <li><a href="#"><i class="ion-social-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--call to action end-->

    <!--footer area start-->
    <footer class="footer_widgets">
        <div class="container">
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="widgets_container contact_us">
                            <div class="footer_logo">
                                <a href="#"><img src="{{ url('assets/img/logo/logo1.png') }}" alt=""></a>
                            </div>
                            <div class="footer_contact">
                                <p>We are providing wide range of two wheeler special tools...</p>
                                <p><span>Address</span> No.1/178, Vadakku Thottam, 
									     Near Nagasakthi Amman Beedam, Malumichampatti, 
									     Coimbatore -641021	</p>
                                <p><span>Need Help?</span>Call: <a href="tel:9003679961">9003679961</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="widgets_container widget_menu">
                            <h3>Information</h3>
                            <div class="footer_menu">
                                <ul>
                                    
                                    <li><a href="{{ url('/terms-and-conditions') }}">Terms & Conditions</a></li>
                                    <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                                    <li><a href="{{ url('/return-policy') }}">Return Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="widgets_container widget_menu">
                            <h3>Extras</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                    <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                                    <li><a href="{{ url('/login') }}">Register</a></li>
                                    <li><a href="{{ url('/login') }}">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="widgets_container" id="newsletter">
                            <h3>Newsletter Subscribe</h3>
                            <p>We’ll never share your email address with a third-party.</p>
                            <div class="subscribe_form">
                                <form  class="mc-form footer-newsletter" method="post" action="{{ url('/subscribe') }}">
                                @csrf    
                                <input type="email" autocomplete="off" name="email" required placeholder="Enter you email address here..." />
                                    <button type="submit">Subscribe</button>
                                </form>
                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts text-centre">
                                    <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                    @if (Session::has('message'))
                                    <div class="mailchimp-success alert alert-success mt-10"> 
                                       
                                           {!! session('message') !!}
                                       
                                    </div><!-- mailchimp-success end -->
                                    @endif
                                    @if (Session::has('sub_error'))
                                    <div class="mailchimp-error alert alert-danger mt-10">{!! session('sub_error') !!}</div><!-- mailchimp-error end -->
                                    @endif
                                </div><!-- mailchimp-alerts end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="copyright_area">
                            <p>Copyright &copy; <?php echo date('Y'); ?> <a href="#">Glanz Special Tools</a> All Right Reserved.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="footer_payment text-right">
                            <a href="#"><img src="assets/img/icon/payment.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer area end-->

    <input type="hidden" id="base_url" value="{{ url('/')}}">
    <input type="hidden" id="csrf_token" value="{{ csrf_token() }}">
    <input type="hidden" id="page" value="{{ url()->current() }}">
    <div id="quick_view_area"></div>

    <div class="toast align-items-center text-white bg-success success-toast border-0 hide no-radius" role="alert" aria-live="assertive" data-bs-autohide="true" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
            <span class="lnr lnr-checkmark-circle"></span> Hello, world! This is a toast message.
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>

