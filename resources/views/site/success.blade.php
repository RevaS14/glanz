
@extends('layouts.default')
@section('content')
<div class="error_section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="error_form">
                        <h2>Order Placed Successfully..</h2>
                        <p>Your order has been placed successfully.<br> Thank you for making order with us you will receive your order soon..</p>
                         
                            <a href="/">Back to Shop</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop