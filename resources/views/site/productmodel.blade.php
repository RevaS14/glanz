<!-- modal area start-->
<div class="modal fade" id="quick_view_box" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal_body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="modal_tab">
                                    <div class="tab-content product-details-large">
                                        @php $i=1; @endphp
                                        @foreach($images as $row)
                                        <?php $image = $row->Image ; ?>
                                        <div class="tab-pane fade show @if($i==1) active @endif" id="tab{{ $i }}" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="{{ $row->Image }}" alt=""></a>
                                            </div>
                                        </div>
                                        @php $i++ @endphp
                                        @endforeach
                                        
                                    </div>
                                    <div class="modal_tab_button">
                                        <ul class="nav product_navactive owl-carousel" role="tablist">
                                            @php $i=1; @endphp
                                            @foreach($images as $row)
                                            <li>
                                                <a class="nav-link @if($i==1) active @endif" data-toggle="tab" href="#tab{{ $i }}" role="tab" aria-controls="tab{{ $i }}" aria-selected="false"><img src="{{ $row->Image }}" alt=""></a>
                                            </li>
                                            @php $i++ @endphp
                                            @endforeach
                                           

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="modal_right">
                                    <div class="modal_title mb-10">
                                        <h2>{{ $product->ProductName }}</h2>
                                    </div>
                                    <div class="modal_price mb-10">
                                        @if($product->Discount > 0)
                                        <span class="new_price">Rs. <?php $discount = ($product->Discount / 100) * $product->Price; echo $price = $product->Price - $discount ?> </span>
                                        <span class="old_price" >Rs. {{ $product->Price }}</span>
                                        @else 
                                        <?php $price = $product->Price; ?>
                                        <span class="new_price">Rs. {{ $product->Price }}</span>
                                        @endif
                                       
                                    </div>
                                    <div class="modal_description mb-15">
                                        <p>{{ $product->Description }}</p>
                                    </div>
                                    <div class="variants_selects">
                                       
                                        
                                        <div class="modal_add_to_cart add_to_cart_area">
                                            <form action="#">
                                                <input class="qty" value="1" type="number">
                                                <button type="submit" class="cart-btn" data-qty="1" data-amount="{{ $price }}" data-name="{{ $product->ProductName }}" data-id="{{ $product->id }}" data-image="{{ $image }}" data-url="{{ $product->ProductLink }}">add to cart</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal_social">
                                        <h2>Share this product</h2>
                                        <ul>
                                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal area end-->