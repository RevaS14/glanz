<div class="container">
            <form action="#">
                <div class="row">
                    <div class="col-12">
                        <div class="table_desc">
                            <div class="cart_page table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                           
                                            <th class="product_thumb">Image</th>
                                            <th class="product_name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product_quantity">Quantity</th>
                                            <th class="product_total">Total</th>
                                            <th class="product_remove">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $total = 0 ?>
                                    @if(session('cart'))
                                        @foreach(session('cart') as $id => $details)
                                            <?php $total += $details['price'] * $details['qty'] ?>
                                        <tr>
                                            <td class="product_thumb">
                                            <a href="{{url('product/'.$details['url'].'')}}"><img class="cart-img" src="{{ url($details['image']) }}" alt="{{ $details['name'] }}"></a>
                                            </td>
                                            <td class="product_name"> <a href="{{url('product/'.$details['url'].'')}}">{{ $details['name'] }}</a></td>
                                            <td class="product-price" data-price="{{ $details['price'] }}">Rs. {{ $details['price'] }}</td>
                                            <td class="product_quantity"><label>Quantity</label> <input data-id="{{ $id }}" min="1" max="100" class="qty" value="{{ $details['qty'] }}" type="number"></td>
                                            <td class="product_total">Rs. {{ $details['qty'] * $details['price'] }}</td>
                                            <td class="product_remove cart_remove"><a href="#" data-id="{{ $id }}" ><i class="fa fa-trash-o"></i></a></td>



                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                          
                        </div>
                    </div>
                </div>
                <!--coupon code area start-->
                <div class="coupon_area">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="coupon_code left">
                                <h3>Coupon</h3>
                                <div class="coupon_inner">
                                    <p>Enter your coupon code if you have one.</p>
                                    <input placeholder="Coupon code" type="text">
                                    <button type="submit">Apply coupon</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="coupon_code right">
                                <h3>Cart Totals</h3>
                                <div class="coupon_inner">
                                    <div class="cart_subtotal">
                                        <p>Subtotal</p>
                                        <p class="cart_amount">Rs. {{ $total }}</p>
                                    </div>
                                    <div class="cart_subtotal ">
                                        <p>Tax</p>
                                        <p class="cart_amount">18% <?php $tax = (18 / 100) * $total; ?></p>
                                    </div>

                                    <div class="cart_subtotal">
                                        <p>Total</p>
                                        <p class="cart_amount">Rs. {{ $total+$tax }}</p>
                                    </div>
                                    <div class="checkout_btn">
                                        <a href="{{ url('/checkout') }}">Proceed to Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--coupon code area end-->
            </form>
        </div>