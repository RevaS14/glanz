@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/') }}">home</a></li>
                            <li>Cart</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shopping_cart_area mt-32">
        {{ $controller->renderCart() }}
    </div>
        @stop