
@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/')}}">home</a></li>
                            <li>My account</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<section class="main_content_area">
        <div class="container">
            <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                            <ul class="nav flex-column dashboard-list" >
                                <li><a href="{{ url('/my-account') }}" data-toggle="tab" class="nav-link ">Dashboard</a></li>
                                <li> <a href="{{ url('/my-account') }}"  class="nav-link active">Orders</a></li>
                                <li><a href="{{ url('/my-account') }}" data-toggle="tab" class="nav-link">Downloads</a></li>
                                <li><a href="{{ url('/my-account') }}" data-toggle="tab" class="nav-link">Addresses</a></li>
                                <li><a href="{{ url('/my-account') }}" data-toggle="tab" class="nav-link">Account details</a></li>
                                <li><a href="{{ url('/logout') }}" class="nav-link">logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            
                            <div class="tab-pane fade  active show" id="orders">
                                <h3>Orders</h3>
                                <div class="row">
                                   
                                <div class="col-md-6">
                                    <div>
                                    <h4>Order ID : {{ $order->OrderNo }} </h4>
                                    <h5>Billing Address :</h5>
                                    <p>{{ $shipping->BillingName }} 
                                     <br>
                                    {{ $shipping->BillingEmail }} 
                                    <br>
                                    {{ $shipping->BillingMobile }} 
                                    <br>
                                    {{ $controller->getCountryName($shipping->BillingCountry) }} 
                                    <br>
                                    {{ $controller->getStateName($shipping->BillingState) }} 
                                    <br>
                                    {{ $controller->getCityName($shipping->BillingCity) }}
                                    <br>
                                    {{ $shipping->BillingAddress1 }}
                                    <br>
                                    {{ $shipping->BillingAddress2 }}
                                    </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                <h4 >Date : {{ date('M d, Y',strtotime($order->created_at)) }}</h4>
                                <h5>Shipping Address :</h5>
                                    <p>{{ $shipping->ShippingName }} 
                                     <br>
                                    {{ $shipping->ShippingEmail }} 
                                    <br>
                                    {{ $shipping->ShippingMobile }} 
                                    <br>
                                    {{ $controller->getCountryName($shipping->ShippingCountry) }} 
                                    <br>
                                    {{ $controller->getStateName($shipping->ShippingState) }} 
                                    <br>
                                    {{ $controller->getCityName($shipping->ShippingCity) }}
                                    <br>
                                    {{ $shipping->ShippingAddress1 }}
                                    <br>
                                    {{ $shipping->ShippingAddress2 }}
                                    </p>
                                </div>
                                </div>
                            </div>
                            <br>
                                <div class="table-responsive">
                                    <table class="table order">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Qty</th>
                                                <th>Discount</th>
                                                <th>Weight</th>
                                                <th>Price</th>
                                                
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $row)
                                            <tr>
                                                <td> <?php $image = $controller->getProductSingleImage($row->ProductId); $pro = $controller->getSingleProduct($row->ProductId); ?>
                                                    <a href="{{ url('/product/'.$row->ProductLink) }}"><img width="50" src="{{ url($image) }}" alt=""></a>
                                                </td>
                                                <td>{{ $row->ProductName }} </td>
                                                <td>{{ $row->Qty }} </td>   
                                                <td>{{ $pro->Discount }}% </td>
                                                <td>{{ $controller->readableMetric($row->Qty * $pro->Weight) }}</td>
                                                <td>Rs. {{ $row->Price }} </td>
                                                
                                                <td style="text-align:right">{{ $row->Total }} </td>                                               
                                            </tr>
                                            @endforeach

                                            
                                        </tbody>
                                        <tfoot id="carttotal">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Cart Subtotal</th>
                                            <td style="text-align:right">{{ $order->Total }}</td>
                                        </tr>
                                        <div >
                                        <?php $tax = (18 / 100) * $order->Total; if($user->State != 35 && $user->State != '') { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>IGST (18%)</th>
                                            <td style="text-align:right"> <?php echo $tax; ?></td>
                                        </tr>
                                        <?php } else { ?>
                                            <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>CGST (9%)</th>
                                            <td style="text-align:right"> <?php echo (9 / 100) * $order->Total; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>SGST (9%)</th>
                                            <td style="text-align:right"> <?php echo (9 / 100) * $order->Total; ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if($order->CourierCharges) { ?> 
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Courier Charges</th>
                                            <td style="text-align:right">
                                                <?php echo $order->CourierCharges; ?>
                                            </td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Packing & Transport Charges</th>
                                            <td style="text-align:right">
                                                <?php echo $order->PackingTransportCharges; ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        
                                        <tr class="order_total">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Order Total</th>
                                            <td style="text-align:right"><strong>Rs. <?php $round = round($order->GrandTotal); ?> {{ $round }}</strong></td>
                                        </tr>
                                        </div>
                                    </tfoot>
                                    </table>
                                </div>
                                <?php if($order->Status=='P') { ?> 
                                    <div class="alert alert-warning">
                                        Your Order is under proceessing
                                    </div>
                                    <?php } else { ?>
                                    <div class="alert alert-success">
                                        Your Order is Completed
                                    </div>
                                    <?php } ?>
                                <a class="btn btn-info" href="{{ url('/my-account') }}">Go Back</a>
                                <a class="pull-right btn btn-success" target="_blank" href="{{ url('/print-invoice/'.$order->id) }}">Print</a>
                            </div>
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @stop