
@extends('layouts.default')
@section('content')
 <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li>about us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--about section area -->
    <div class="about_section mt-32">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                  

                    <div class="about_content">
                        <h1>Welcome to Glanz Special Tools!</h1>
                        <p>We are Special Tool Manufacturer & Supplier to Manufacturing Companies in all major sectors like Automobiles ... We also design & manufacture Special Tools.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--about section end-->

    <!--chose us area start-->
    <div class="choseus_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="chose_title">
                        <h1>Why chose us?</h1>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone">
                        <span class="lnr lnr-star"></span>
                        </div>
                        <div class="chose_content">
                            <h3>Customer Satisfaction</h3>
                            <p>We are working harder for aim to get 100% customer satisfaction to make them happier and we work towards their requirements. </p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone">
                        <span class="lnr lnr-diamond"></span>
                        </div>
                        <div class="chose_content">
                            <h3>100% Quality</h3>
                            <p>We sure about our product quality will make you buy more and more tools from us. We are providing branded hight quality tools.</p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone">
                        <span class="lnr lnr-rocket"></span>
                        </div>
                        <div class="chose_content">
                            <h3>Support 24/7</h3>
                            <p>We always happy to support you that designing and manufacturing tools and we try to give the tools as per your time needs.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--chose us area end-->

    <!--services img area-->
    <div class="about_gallery_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_gallery_section">
                        <div class="chose_icone text-center">
                        <span class="lnr lnr-briefcase"></span>
                        </div>
                        <div class="about_gallery_content">
                            <h3>What do we do?</h3>
                            <p>Glanz Special Tools Manufacturers presently is considered to be a leading company in India, engaged in design, manufacture & supply a wide range of standard and special tools in world wide.</p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_gallery_section">
                    <div class="chose_icone text-center">
                        <span class="lnr lnr-flag"></span>
                        </div>
                        <div class="about_gallery_content">
                            <h3>Our Mission</h3>
                            <p>Our Mission is to give good improved quality products in whatever the hard situation we try to meet our customers time duration and meet customers expectation.</p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_gallery_section">
                        <div class="chose_icone text-center">
                        <span class="lnr lnr-store"></span>
                        </div>
                        <div class="about_gallery_content">
                            <h3>History Of Us</h3>
                            <p>Glanz Special Tools located in Coimbatore is successfully entered into the industry with its humble beginnings in the year 2012. Our core proficiency is manufacture and supply of special tools all over the world</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--services img end-->

    <!--testimonials section start-->
    <div class="testimonial_are">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="testimonial_titile">
                        <h1>What Our Custumers Say ?</h1>
                    </div>
                </div>
                <div class="testimonial_active owl-carousel">
                    <div class="col-12">
                        <div class="single_testimonial">
                            <p>These guys have been absolutely outstanding. Perfect Tools and the best of all that you have many options to choose! Best Support team ever! Very fast responding! Thank you very much! I highly recommend these people!</p>
                            <img src="assets/img/about/testimonial4.jpg" alt="">
                            <span class="name">Krishnan</span>
                            <span class="job_title">Malatesh Enterprises</span>
                            <div class="product_ratting">
                                <ul>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                        <div class="single_testimonial">
                            <p>These guys have been absolutely outstanding. Perfect Themes and the best of all that you have many options to choose! Best Support team ever! Very fast responding! Thank you very much! I highly recommend this theme and these people!</p>
                            <img src="assets/img/about/testimonial5.jpg" alt="">
                            <span class="name">Kathy Young</span>
                            <span class="job_title">CEO of SunPark</span>
                            <div class="product_ratting">
                                <ul>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                    
                </div>
            </div>
        </div>
    </div>
    <!--testimonials section end-->
    <style>
        .chose_icone .lnr {
            color:#e7eb09;
            font-size: 56px;
        }
        .chose_icone  {
            margin-top:10px;
            margin-bottom:10px;
        }
    </style>
    
    @stop