
@extends('layouts.default')
@section('content')
<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/') }}">home</a></li>
                            <li>{{ $category->CategoryName }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="shop_area shop_reverse">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <!--sidebar widget start-->
                    <aside class="sidebar_widget">
                        <div class="widget_inner">
                           
                            <div class="widget_list widget_categories">
                                <h2>categories</h2>
                                <ul>
                                    @foreach($categories as $user)
                                    <li><a href="{{ url('/category/'.$user->CategoryLink) }}"> {{ $user->CategoryName }}</a>
                                    <span class="checkmark"></span>
                                    </li>
                                    @endforeach
                                    

                                </ul>
                            </div>

                            
                            
                        </div>
                        <div class="shop_sidebar_banner">
                            <a href="#"><img src="assets/img/bg/banner9.jpg" alt=""></a>
                        </div>
                    </aside>
                    <!--sidebar widget end-->
                </div>
                <div class="col-lg-9 col-md-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                    
                    <div class="shop_title">
                        <h1>{{ $category->CategoryName }}</h1>
                    </div>
                   
                    <!--shop toolbar end-->

                    <div class="row shop_wrapper grid_4">
                                @php
                                $i = 1
                                @endphp
                                @foreach($products as $product)
                                <div class="col-lg-3 col-md-4 col-sm-6">
                                <?php echo $controller->getFeatured($product); ?>
                                </div>
                                @php
                                $i++
                                @endphp
                               
                            @endforeach
                        
                        
                    </div>

                    
                    
                        <div class="pagination">
                        <?php echo $products->render(); ?>
                            
                        </div>
                   
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>
            </div>
        </div>
    </div>
    @stop