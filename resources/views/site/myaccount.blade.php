
@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li>My account</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<section class="main_content_area">
        <div class="container">
            <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                            <ul role="tablist" class="nav flex-column dashboard-list" id="nav-tab">
                                <li><a href="#dashboard" data-toggle="tab" class="nav-link active">Dashboard</a></li>
                                <li> <a href="#orders" data-toggle="tab" class="nav-link">Orders</a></li>
                                <li><a href="#account-details" data-toggle="tab" class="nav-link">Account details</a></li>
                                <li><a href="#change-password" data-toggle="tab" class="nav-link">Change Password</a></li>
                                <li><a href="login.html" class="nav-link">logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                    @if(Session::has('success'))
                    <div class="alert alert-success">
                    <span class="lnr lnr-checkmark-circle"></span>
                    {{ Session::get('success') }}
                    </div>
                    @endif

                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                    <span class="lnr lnr-warning"></span> 
                    {{ Session::get('error') }}
                    </div>
                    @endif
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
                            <div class="tab-pane fade active show" id="dashboard">
                                <h3>Dashboard </h3>
                                <h2>Welcome {{ $user->name }}!</h2>
                                <p>From your account dashboard. you can easily check &amp; view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a> and <a href="#">Edit your password and account details.</a></p>
                            </div>
                            <div class="tab-pane fade" id="orders">
                                <h3>Orders</h3>
                                <div class="table-responsive ">
                                    <table class="table order" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>Order#</th>
                                                <th>Date</th>
                                                
                                                <th>Net Amount</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $row)
                                            <tr>
                                                <td>{{ $row->OrderNo }}</td>
                                                <td>{{ date('M d, Y',strtotime($row->created_at)) }}</td>
                                                
                                                <td>Rs. {{ $row->GrandTotal }} </td>                                               
                                                <td><?php if($row->Status=='C') { ?> <span class="success">Completed</span> <?php } else { ?> <span class="pending">Pending</span>  <?php } ?></td>
                                                <td><a href="{{ url('/view-order/'.$row->id) }}" class="view">view</a></td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                           
                            
                            <div class="tab-pane fade" id="account-details">
                                <h3>Account details </h3>
                                <div class="login">
                                    <div class="login_form_container">
                                        <div class="account_login_form">
                                            <form action="{{ url('/edit-account') }}" method="post">
                                            @csrf 
                                                <label>Name</label>
                                                <input type="text" name="name" required value="{{ $user->name }}">
                                               
                                                <label>Email</label>
                                                <input type="email" name="email" disabled required value="{{ $user->email }}">
                                                <label>Mobile</label>
                                                <input type="text" name="mobile" required value="{{ $user->mobile }}">
                                                <label>Country</label>
                                            
                                                <select class="form-control mb-3" name="country" id="country" required>
                                                    <option value="">Select</option>
                                                @foreach($countries as $row) 
                                                    <option <?php if($user) { if($user->country) { if($user->country == $row->id) { echo 'selected'; }  }  } ?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>

                                              
                                                    <label for="country">State </label>
                                                    <select class="selectize-select form-control mb-3" name="state" id="state" required>
                                                    <option value="">Select</option>
                                                    @foreach($states as $row) 
                                                        <option <?php if($user) { if($user->state) { if($user->state == $row->id) { echo 'selected'; }  }  } ?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                    </select>
                                              
                                                    <label for="country">City <span>*</span></label>
                                                    <select class="selectize-select form-control mb-3"  name="city" id="city" required>
                                                    <option value="">Select</option>
                                                    @foreach($cities as $row) 
                                                        <option <?php if($user) { if($user->city) { if($user->city == $row->id) { echo 'selected'; }  }  } ?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                    @endforeach
                                                    </select>
                                                <label>Address Line 1</label>
                                                <input type="text" name="address1" required value="{{ $user->address_1 }}">
                                                <label>Address Line 2</label>
                                                <input type="text" name="address2" value="{{ $user->address_2 }}">
                                                <label>Pincode</label>
                                                <input type="text" name="pincode" required value="{{ $user->pincode }}">
                                               
                                               
                                                <div class="save_button primary_btn default_button">
                                                    <button type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="change-password">
                                <h3>Change Password </h3>
                                <div class="login">
                                    <div class="login_form_container">
                                        <div class="account_login_form">
                                            <form action="{{ url('/change-password') }}" method="post">
                                            @csrf 
                                                <label>Current Password</label>
                                                <input type="password" name="cpassword" required>
                                               
                                                <label>New Password</label>
                                                <input type="password" name="npassword" required>
                                                <label>Confirm New Password</label>
                                                <input type="password" name="confirmpassword" required>
                                                
                                                <div class="save_button primary_btn default_button">
                                                    <button type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @stop