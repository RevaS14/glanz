@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/') }}">home</a></li>
                            <li>Wish List</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shopping_cart_area mt-32">
    <div class="container">
    @if(Session::has('success'))
            <div class="alert alert-success">
            <span class="lnr lnr-checkmark-circle"></span>
            {{ Session::get('success') }}
            </div>
            @endif
            <form action="#">
                <div class="row">
                    <div class="col-12">
                       
                        <?php if(count($list) > 0)  { ?>
                            <div class="table_desc">
                            <div class="cart_page table-responsive">
                          
                                <table>
                                    <thead>
                                        <tr>
                                           
                                            <th class="product_thumb">Image</th>
                                            <th class="product_name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product_quantity">Discount</th>
                                            <th class="product_remove">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $total = 0 ?>
                                   
                                        @foreach($list as $row)
                                            <?php $pro = $controller->getSingleProduct($row->ProductId); $image = $controller->getProductSingleImage($row->ProductId);  ?>
                                        <tr>
                                            <td class="product_thumb">
                                            <a href="{{url('product/'.$pro->ProductLink.'')}}"><img class="cart-img" src="{{ url($image) }}" alt="{{ $pro->ProductName }}"></a>
                                            </td>
                                            <td class="product_name"> <a href="{{url('product/'.$pro->ProductLink.'')}}">{{ $pro->ProductName }}</a></td>
                                            <td class="product_quantity">Rs. {{ $pro->Price }}</td>
                                            <td class="product_total">{{ $pro->Discount }} %</td>
                                            <td class="product_remove"><a href="{{url('wishlist-remove/'.$row->id.'')}}"><i class="fa fa-trash-o"></i></a></td>



                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                            </div>
                            </div>
                            <?php } else { ?> 
                                <h4>No poducts in your wishlist..</h4>
                                <br>
                            <?php } ?>
                          
                       
                    </div>
                </div>
                <!--coupon code area start-->
                
            </form>
        </div>
    </div>
        @stop