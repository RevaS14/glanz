
@extends('layouts.default')
@section('content')
<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/') }}">home</a></li>
                            <li><a href="{{ url('/category/'.$category->CategoryLink) }}">{{ $category->CategoryName }}</a></li>
                            <li>{{ $product->ProductName }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product_details mt-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product-details-tab">

                        <div id="img-1" class="zoomWrapper single-zoom">
                            <a href="#">
                            <?php $image = $controller->getProductSingleImage($product->id); ?>
                                <img id="zoom1" src="{{ url($image) }}" data-zoom-image="{{ url($image) }}" alt="big-1">
                            </a>
                        </div>

                        <div class="single-zoom-thumb">
                            <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                                @php $i=1; @endphp
                                @foreach($productImages as $row)
                                <li>
                                    <a class="elevatezoom-gallery @if($i==1) active @endif" data-update="" data-image="{{ url($row->Image) }}" data-zoom-image="{{ url($row->Image) }}">
                                    <img src="{{ url($row->Image) }}" alt="">
                                    </a>
                                </li>
                                @php $i++ @endphp
                                @endforeach
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product_d_right">
                        <form action="#">

                            <h1>{{ $product->ProductName }}</h1>
                           
                            <div class=" product_ratting">
                            <ul>
                                <?php
                                $totalRating = 5;
                                for ($k = 1; $k <= $totalRating; $k++) {
                                    if($overall_rating < $k ) {
                                        if(is_float($overall_rating) && (round($overall_rating) == $k)){
                                            echo '<li><a href=""><i class="fa fa-star-half-o"></i></a></li>';
                                        }else{
                                            echo '<li><i class="fa fa-star gray-color"></i></li>';
                                        }
                                     }else {
                                        echo '<li><a href=""><i class="fa fa-star"></i></a></li>';
                                     }
                                     
                                }
                                ?>
                                <li>  <a style="color:#82837b" href="#nav-tab">(<?php echo $overall_rating; ?> Customer Review) </a></li>
                                </ul>

                                

                            </div>
                            @if($product->Discount > 0)
                            <div class="price_box">
                                <span class="current_price">Rs. <?php $discount = ($product->Discount / 100) * $product->Price; echo $price = $product->Price - $discount ?> </span>
                                <span class="old_price">Rs. {{ $product->Price }}</span>
                            </div>
                            @else 
                            <div class="price_box">
                                <span class="regular_price">Rs. {{ $product->Price }} </span>
                                <?php $price = $product->Price; ?>
                            </div>
                            @endif
                            
                            <div class="product_desc">
                                <?php echo $product->Description; ?>
                            </div>

                            
                            <div class="product_variant product_qty quantity add_to_cart_area">
                                <label>quantity</label>
                                <input min="1" class="qty" value="1" type="number">
                                <button class="button cart-btn" type="submit" data-qty="1" data-price="{{ $price }}" data-id="{{ $product->id }}" data-name="{{ $product->ProductName }}" data-url="{{ $product->ProductLink }}" data-image="{{ $image }}" style="margin-top:0">add to cart</button>

                            </div>
                            <div class=" product_d_action">
                                <ul>
                                    <li><a href="#" title="Add to wishlist">+ Add to Wishlist</a></li>
                                </ul>
                            </div>
                          

                        </form>
                        <div class="priduct_social">
                            <ul>
                                <li><a class="facebook" href="#" title="facebook"><i class="fa fa-facebook"></i> Like</a></li>
                                <li><a class="twitter" href="#" title="twitter"><i class="fa fa-twitter"></i> tweet</a></li>
                                <li><a class="pinterest" href="#" title="pinterest"><i class="fa fa-pinterest"></i> save</a></li>
                                <li><a class="google-plus" href="#" title="google +"><i class="fa fa-google-plus"></i> share</a></li>
                                <li><a class="linkedin" href="#" title="linkedin"><i class="fa fa-linkedin"></i> linked</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product_d_info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">
                        <div class="product_info_button">
                            <ul class="nav" role="tablist" id="nav-tab">
                                <li>
                                    <a class="" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                                </li>
                               
                                <li>
                                    <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="true" class="active">Reviews ({{ $all_ratings_count = count($ratings) }})</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade" id="info" role="tabpanel">
                                <div class="product_info_content">
                                    <p>{{ $product->Description }}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sheet" role="tabpanel">
                                <div class="product_d_table">
                                    <form action="#">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="first_child">Compositions</td>
                                                    <td>Polyester</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Styles</td>
                                                    <td>Girly</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Properties</td>
                                                    <td>Short Dress</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="product_info_content">
                                    <p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
                                </div>
                            </div>

                            <div class="tab-pane fade active show" id="reviews" role="tabpanel">
                                <div class="reviews_wrapper">
                                    <h2>{{ $all_ratings_count = count($ratings) }} review for {{ $product->ProductName }}</h2>
                                    @foreach($ratings as $star)
                                    <div class="reviews_comment_box">
                                        <div class="comment_thmb">
                                            <i class="fa fa-comment-o fa-2x"></i>
                                        </div>
                                        <div class="comment_text">
                                            <div class="reviews_meta">
                                                <div class="star_rating">
                                                    <ul>
                                                        <?php for($i=1; $i<=$star->Star; $i++) { ?>
                                                        <li><a href="#"><i class="ion-ios-star"></i></a></li>
                                                        <?php } ?>

                                                        <?php for($j=1; $j<=5-$star->Star; $j++) { ?>
                                                        <li><i class="ion-ios-star gray-color"></i></li>
                                                        <?php } ?>
                                                       
                                                    </ul>
                                                </div>
                                                <p><strong><?php $user = $controller->getUser($star->UserId); ?> <?php echo $user->name; ?> </strong>- {{ date('F d, Y',strtotime($star->created_at)) }}</p>
                                                <span>{{ $star->Review }}</span>
                                            </div>
                                        </div>

                                    </div>
                                    @endforeach
                                    
                                    <div class="comment_title">
                                        <h2>Add a review </h2>
                                    </div>
                                    @if(Session::has('error'))
                                    <div class="alert alert-danger">
                                    <span class="lnr lnr-warning"></span> 
                                    {{ Session::get('error') }}
                                    </div>
                                    @endif

                                    @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    <span class="lnr lnr-checkmark-circle"></span>
                                    {{ Session::get('success') }}
                                    </div>
                                    @endif
                                    <form method="post" action="{{ url('/review') }} ">
                                    @csrf
                                    <div class="product_ratting mb-10">
                                        <input type="hidden" name="productId" value="{{ $product->id }}">
                                        <h3>Your rating</h3>
                                        <div class="stars">
                                        
                                            <input class="star star-5" id="star-5" type="radio" value="1" name="star" required/>
                                            <label class="star star-5" for="star-5"></label>
                                            <input class="star star-4" id="star-4" type="radio" value="2" name="star"/>
                                            <label class="star star-4" for="star-4"></label>
                                            <input class="star star-3" id="star-3" type="radio" value="3" name="star"/>
                                            <label class="star star-3" for="star-3"></label>
                                            <input class="star star-2" id="star-2" type="radio" value="4" name="star"/>
                                            <label class="star star-2" for="star-2"></label>
                                            <input class="star star-1" id="star-1" type="radio" value="5" name="star"/>
                                            <label class="star star-1" for="star-1"></label>

                                        </div>
                                    </div>
                                    <div class="product_review_form">
                                       
                                            <div class="row">
                                                <div class="col-12">
                                                    <label for="review_comment">Your review </label>
                                                    <textarea name="review" id="review_comment" required></textarea>
                                                </div>
                                               
                                            </div>
                                            <button type="submit">Submit</button>
                                       
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <!--product area start-->
   <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong>Related</strong>Products</span></h2>
                    </div>
                    <div class="product_carousel product_column5 owl-carousel">
                            @php
                                $i = 1
                                @endphp
                                @foreach($relatedProducts as $product)
                                <?php echo $controller->getFeatured($product); ?>
                                @php
                                $i++
                                @endphp
                               
                            @endforeach
                        
                        
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--product area end-->
@stop