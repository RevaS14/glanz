@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li>Login</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="customer_login mt-32">
        <div class="container">
            <div class="row">
            @if(Session::has('error'))
            <div class="alert alert-danger">
            <span class="lnr lnr-warning"></span> 
            {{ Session::get('error') }}
            </div>
            @endif

            @if(Session::has('success'))
            <div class="alert alert-success">
            <span class="lnr lnr-checkmark-circle"></span>
            {{ Session::get('success') }}
            </div>
            @endif
                <!--login area start-->
                <div class="col-lg-6 col-md-6">
                    <div class="account_form">
                        <h2>login</h2>
                        <form action="{{ url('/user-login') }} " method="post">
                            @csrf
                            <p>
                                <label>Email or Mobile Number<span>*</span></label>
                                <input type="text" id="username" name="username" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>">
                            </p>
                            <p>
                                <label>Password <span>*</span></label>
                                <input type="password" id="password" name="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>">
                            </p>
                            <div class="login_submit">
                                <a href="#">Lost your password?</a>
                                <label for="remember">
                                    <input id="remember" type="checkbox" name="remember_me" <?php if(isset($_COOKIE["username"])) { echo 'checked'; } ?>>
                                    Remember me
                                </label>
                                <input type="hidden" name="page" value="login">
                                <button type="submit">login</button>

                            </div>

                        </form>
                    </div>
                </div>
                <!--login area start-->

                <!--register area start-->
                <div class="col-lg-6 col-md-6">
                    <div class="account_form register">
                        <h2>Register</h2>
                        <form action="{{ url('/register')}}" method="post">
                        @csrf
                            <p>
                                <label>Name <span class="text-danger">*</span></label>
                                <input type="text" name="username" required>
                            </p>
                            <p>
                                <label>Email address<span class="text-danger">*</span></label>
                                <input type="text" name="email" required>
                            </p>
                            <p>
                                <label>Mobile <span class="text-danger">*</span></label>
                                <input type="text" name="mobile" required>
                            </p>
                            <p>
                                <label>Password <span class="text-danger">*</span></label>
                                <input type="password" name="password" required>
                            </p>
                            <p>
                                <label>Confirm Password <span class="text-danger">*</span></label>
                                <input type="password" name="cpassword" required>
                            </p>
                            <div class="login_submit">
                                <button type="submit">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--register area end-->
            </div>
        </div>
    </div>

@stop