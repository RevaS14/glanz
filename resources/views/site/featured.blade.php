                <?php use App\Http\Controllers\SiteController; $controller = new SiteController(); ?>       
                        <div class="single_product">
                                <div class="product_name">
                                    <h3><a href="{{ url('/product/'.$product->ProductLink) }}">{{ $product->ProductName }} </a></h3>
                                    <p class="manufacture_product"><a href="#">Accessories</a></p>
                                </div>
                                <div class="product_thumb">
                                 <?php $image = $controller->getProductSingleImage($product->id); ?>
                                    <a class="primary_img" href="{{ url('/product/'.$product->ProductLink) }}"><img src="{{ url($image) }}" alt=""></a>
                                    @if($product->Discount > 0) 
                                    <div class="label_product">
                                        <span class="label_sale" data-discount="{{ $product->Discount }}">{{ $product->Discount }}%</span>
                                    </div>
                                    @endif

                                    <div class="action_links">
                                        <?php $checkWishList = $controller->checkWishList($product->id); ?>
                                        <ul>
                                            <li class="quick_button"><a href="#" class="quick_view" id="{{ $product->id }}"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                            <li class="wishlist"><a href="#" class="<?php echo $checkWishList; ?>" id="{{ $product->id }}" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <?php $overall_rating = $controller->overall_rating($product->id); ?>
                                    <div class="product_ratings">
                                        <ul>
                                        <?php
                                            $totalRating = 5;
                                            for ($k = 1; $k <= $totalRating; $k++) {
                                                if($overall_rating < $k ) {
                                                    if(is_float($overall_rating) && (round($overall_rating) == $k)){
                                                        echo '<li><a href=""><i class="fa fa-star-half-o"></i></a></li>';
                                                    }else{
                                                        echo '<li><i class="fa fa-star gray-color"></i></li>';
                                                    }
                                                }else {
                                                    echo '<li><a href=""><i class="fa fa-star"></i></a></li>';
                                                }
                                                
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="product_footer d-flex align-items-center">
                                        @if($product->Discount > 0)
                                        <div class="price_box">
                                            <?php $discount = ($product->Discount / 100) * $product->Price; ?>
                                            <span class="current_price" data-price="<?php echo $price = $product->Price - $discount ?>">Rs. <?php echo $price = $product->Price - $discount ?> </span>
                                            <span class="old_price" data-price="{{ $product->Price }}">Rs. {{ $product->Price }}</span>
                                        </div>
                                        @else 
                                        <div class="price_box">
                                            <span class="regular_price" data-price="{{ $product->Price }}">Rs. {{ $product->Price }} </span>
                                            <?php $price = $product->Price; ?>
                                        </div>
                                        @endif
                                      
                                    </div>
                                    <div class="product_qty add_to_cart_area">
                                        <label>Qty:</label>
                                        <input type="number" value="1" class="qty form-control">
                                        <button type="submit" class="btn btn-success cart-btn" data-qty="1" data-price="{{ $price }}" data-weight="{{ $product->Weight }}" data-id="{{ $product->id }}" data-name="{{ $product->ProductName }}" data-url="{{ $product->ProductLink }}" data-image="{{ $image }}"><span class="lnr lnr-cart"></span> Add to cart</button>
                                    </div>
                                </div>
                            </div>