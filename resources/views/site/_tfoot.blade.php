
                <tr>
                    <td></td>
                    <td></td>
                    <th>Cart Subtotal</th>
                    <td>{{ $total }}</td>
                </tr>
                <?php $tax = (18 / 100) * $total; if($state != 35 && $state != '') { ?>
                <tr>
                    <td></td>
                    <td></td>
                    <th>IGST (18%)</th>
                    <td> <?php echo $tax; ?></td>
                </tr>
                <?php } else { ?>
                    <tr>
                    <td></td>
                    <td></td>
                    <th>CGST (9%)</th>
                    <td> <?php echo (9 / 100) * $total; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <th>SGST (9%)</th>
                    <td> <?php echo (9 / 100) * $total; ?></td>
                </tr>
                <?php } ?>
                <?php $dtype = Session::get('deliveryType'); $dcharge = $weight * 75; if($state == 35) { $dcharge = $weight * 100; } if($dcharge < 100 || $dtype == 'courier') { ?> 
                <tr>
                    <td></td>
                    <td></td>
                    <th>Courier Charges</th>
                    <td>
                        <?php echo $dcharge; ?>
                    </td>
                </tr>
                <?php } else { ?>
                <tr>
                    <td></td>
                    <td></td>
                    <th>Packing & Transport Charges</th>
                    <td>
                        <?php $dcharge = 200 + $weight * 5 ; echo $dcharge; ?>
                    </td>
                </tr>
                <?php } ?>
                <input type="hidden" name="dcharge" id="dcharge" value="<?php echo $dcharge; ?>">
                <input type="hidden" name="weight" id="weight" value="<?php echo $weight; ?>">
                <input type="hidden" name="total" id="total" value="<?php echo $total; ?>">
                <tr class="order_total">
                    <td></td>
                    <td></td>
                    <th>Order Total</th>
                    <td><strong>Rs. {{ $total+$tax+$dcharge }}</strong></td>
                </tr>