<div class="single_product">
    <div class="product_content">
        <h3><a href="product-details.html">{{ $product->ProductName }}</a></h3>
        <div class="product_ratings">
        <?php $overall_rating = $controller->overall_rating($product->id); ?>
            <ul>
            <?php
                $totalRating = 5;
                for ($k = 1; $k <= $totalRating; $k++) {
                    if($overall_rating < $k ) {
                        if(is_float($overall_rating) && (round($overall_rating) == $k)){
                            echo '<li><a href=""><i class="fa fa-star-half-o"></i></a></li>';
                        }else{
                            echo '<li><i class="fa fa-star gray-color"></i></li>';
                        }
                    }else {
                        echo '<li><a href=""><i class="fa fa-star"></i></a></li>';
                    }
                    
                }
                ?>
            </ul>
        </div>
        @if($product->Discount > 0)
        <div class="price_box">
            <span class="current_price">Rs. <?php $discount = ($product->Discount / 100) * $product->Price; echo $product->Price - $discount ?> </span>
            <span class="old_price">Rs. {{ $product->Price }}</span>
        </div>
        @else 
        <div class="price_box">
            <span class="regular_price">Rs. {{ $product->Price }} </span>
        </div>
        @endif
    </div>
    <div class="product_thumb">
        <a class="primary_img" href="product-details.html"><img src="{{ $controller->getProductSingleImage($product->id) }}" alt=""></a>
    </div>
</div>