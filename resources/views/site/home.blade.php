<?php use App\Http\Controllers\SiteController; $controller = new SiteController(); ?>

@extends('layouts.default')
@section('content')
 <!--slider area start-->
    <section class="slider_section slider_two mb-50">
        <div class="slider_area owl-carousel">
        @foreach($sliders as $slider)
            <div class="single_slider d-flex align-items-center" data-bgimg="{{ $slider->ImagePath }}">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="slider_content">
                                <h2>{{ $slider->Caption }}</h2>
                                <h1>{{ $slider->Description }}</h1>
                                <a class="button" href="{{ $slider->Link }}">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            
            
        </div>
    </section>
    <!--slider area end-->

    <!--shipping area start-->
    <section class="shipping_area mb-50">
        <div class="container">
            <div class=" row">
                <div class="col-12">
                    <div class="shipping_inner">
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/img/about/shipping1.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Fast Shipping</h2>
                                <p>Fast shipping on all your order</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/img/about/shipping2.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Support 24/7</h2>
                                <p>Contact us 24 hours a day</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/img/about/shipping3.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>100% Quality</h2>
                                <p>We are providing 100% quality tools</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/img/about/shipping4.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Payment Secure</h2>
                                <p>We ensure for secure payment</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--shipping area end-->
    <!--product area start-->
    <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong>Our</strong>Products</span></h2>
                        <ul class="product_tab_button nav" role="tablist" id="nav-tab">
                          
                        </ul>
                    </div>

                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="brake" role="tabpanel">
                    <div class="product_carousel product_column5 owl-carousel">
                       
                            @php
                            $i = 0
                            @endphp
                            @foreach($homeProducts as $product)
                                @if($i % 2 == 0)
                                <div class="single_product_list">
                                @endif
                                <?php echo $controller->getFeatured($product); ?>
                                
                               
                                @php
                                $i++
                                @endphp
                                @if($i % 2 == 0)
                                </div>
                                @endif
                            @endforeach
                            
                            
                   
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>
    <!--product area end-->


    <!--product area start-->
    <section class="new_product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_tab_button tab_button2">
                        <ul class="nav" role="tablist" id="nav-tab2">
                            <li>
                                <a class="active" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="true"><span>Most Viewd</span> Products</a>
                            </li>
                           
                        </ul>
                    </div>

                </div>
            </div>



            <div class="tab-content">
                <div class="tab-pane fade show active" id="featured" role="tabpanel">
                    <div class="new_product_container">
                    <?php $i=1; ?>
                        @foreach($mostViewdProducts as $product)
                        @if($i==1) 
                        <div class="sample_product">
                            <div class="product_name">
                                <h3><a href="product-details.html">{{ $product->ProductName }}</a></h3>
                                <div class="manufacture_product">
                                    <p><a href="#">Hewlett-Packard</a></p>
                                </div>
                            </div>
                            <div class="product_thumb">
                            <?php $image = $controller->getProductSingleImage($product->id); ?>
                                <a href="#"><img src="{{ $image }}" alt=""></a>
                            </div>
                            <div class="product_content">
                            <?php $overall_rating = $controller->overall_rating($product->id); ?>
                                <div class="product_ratings">
                                    <ul>
                                    <?php
                                        $totalRating = 5;
                                        for ($k = 1; $k <= $totalRating; $k++) {
                                            if($overall_rating < $k ) {
                                                if(is_float($overall_rating) && (round($overall_rating) == $k)){
                                                    echo '<li><a href=""><i class="fa fa-star-half-o"></i></a></li>';
                                                }else{
                                                    echo '<li><i class="fa fa-star gray-color"></i></li>';
                                                }
                                            }else {
                                                echo '<li><a href=""><i class="fa fa-star"></i></a></li>';
                                            }
                                            
                                        }
                                    ?>
                                    </ul>
                                </div>
                                    @if($product->Discount > 0)
                                    <div class="price_box">
                                        <span class="current_price">Rs. <?php $discount = ($product->Discount / 100) * $product->Price; echo $product->Price - $discount ?> </span>
                                        <span class="old_price">Rs. {{ $product->Price }}</span>
                                    </div>
                                    @else 
                                    <div class="price_box">
                                        <span class="regular_price">Rs. {{ $product->Price }} </span>
                                    </div>
                                    @endif
                                <div class="quantity_progress">
                                    <p class="product_available">Availabe: <span>{{ $product->Stock }}</span></p>
                                </div>
                                <div class="bar_percent">

                                </div>
                            </div>

                        </div>
                        @endif
                        <?php $i++; ?>
                        @endforeach
                        <div class="product_carousel product_bg  product_column2 owl-carousel">
                            @php
                            $i = 0
                            @endphp
                            
                            @foreach($mostViewdProducts as $product)
                                @if($i % 3 == 0)
                                <div class="small_product">
                                @endif
                                <?php echo $controller->getMostViewed($product); ?>
                                @php
                                $i++
                                @endphp
                                @if($i % 3 == 0)
                                </div>
                                @endif

                            @endforeach
                      
                            
                            </div>
                            
                            
                            
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>
    <!--product area end-->
    <!--banner area start-->
    <section class="banner_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_container">
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="#"><img src="assets/img/bg/banner3.jpg" alt=""></a>
                                <div class="banner_text">
                                    <h3>All Kind</h3>
                                    <h2>Clutch Nut Spanner</h2>
                                    <a href="shop.html">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="#"><img src="assets/img/bg/banner4.jpg" alt=""></a>
                                <div class="banner_text">
                                    <h3>All - New</h3>
                                    <h2>Perfomance Tools</h2>
                                    <a href="shop.html">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--banner area end-->

    <!--product area start-->
    <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span> <strong>Special</strong>Offers</span></h2>
                    </div>
                    <div class="product_carousel product_column5 owl-carousel">
                            @foreach($offeredProducts as $product)
                                <?php echo $controller->getFeatured($product); ?>
                            @endforeach
                        
                        
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--product area end-->

    <!--banner area start-->
    <section class="banner_area mb-50" style="background: #f1f1f1;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single_banner banner_fullwidth">
                        <div class="banner_thumb">
                            <a href="#"><img src="assets/img/bg/banner5.jpg" alt=""></a>
                            <div class="banner_text">
                                <h2>Discover Brand New</h2>
                                <h3>Special Tools</h3>
                                <p>Glanz Special Tools are expert in design and manufacturing</p>
                                <a href="shop.html">Discover Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--banner area end-->

    
    
    <!--brand area start-->
    <div class="brand_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="brand_container owl-carousel">
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand.png" alt=""></a>
                        </div>
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                        </div>
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                        </div>
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand3.png" alt=""></a>
                        </div>
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand4.png" alt=""></a>
                        </div>
                        <div class="single_brand">
                            <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand area end-->
    @stop