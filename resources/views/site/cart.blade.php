
            <div class="cart_close">
                <div class="cart_text">
                    <h3>cart</h3>
                </div>
                <div class="mini_cart_close">
                    <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                </div>
            </div>
            <?php $total = 0 ?>
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                <?php $total += $details['price'] * $details['qty'] ?>
            <div class="cart_item">
                <div class="cart_img">
                    <a href="{{url('product/'.$details['url'].'')}}"><img src="{{ url($details['image']) }}" alt="{{ $details['name'] }}"></a>
                </div>
                <div class="cart_info">
                    <a href="{{url('product/'.$details['url'].'')}}">{{ $details['name'] }}</a>

                <span class="quantity">Qty: {{ $details['qty'] }}</span>
                    <span class="price_cart">Rs. {{ $details['price'] }}</span>

                </div>
                <div class="cart_remove">
                    <a href="#" data-id="{{ $id }}" ><i class="ion-android-close"></i></a>
                </div>
            </div>
            @endforeach
        @endif
        @if(session('cart'))
            <div class="mini_cart_table">
                <div class="cart_total">
                    <span>Sub Total:</span>
                    <span class="price">Rs. {{ $total }} </span>
                </div>
                <div class="cart_total">
                    <span>Tax:</span>
                    <span class="price"> 18% <?php $tax = (18 / 100) * $total; ?></span>
                </div>
                <div class="cart_total mt-10">
                    <span class="price">Total:</span>
                    <span class="price">Rs. {{ $total+$tax }}</span>
                </div>
            </div>

            <div class="mini_cart_footer">
                <div class="cart_button">
                    <a href="{{ url('/cart') }}">View cart</a>
                </div>
                
                <div class="cart_button">
                    <a class="active" href="{{ url('/checkout') }}">Checkout</a>
                </div>

            </div>
            @else
            <div class="mini_cart_footer">
                <div class="cart_button">
                    <p class="text-center mt-10" > No item in your cart</p>
                </div>
            </div>
        @endif
