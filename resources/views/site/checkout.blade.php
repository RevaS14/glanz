@extends('layouts.default')
@section('content')

<div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{ url('/') }}">home</a></li>
                            <li>Checkout</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Checkout_section mt-32">
        <div class="container">
        @if(!Session::get('user_id'))
            <div class="row">
                <div class="col-12">
                    <div class="user-actions">
                        <h3> 
                            <i class="fa fa-file-o" aria-hidden="true"></i>
                            Returning customer?
                            <a class="Returning collapsed" href="#checkout_login" data-bs-toggle="collapse" aria-expanded="false">Click here to login</a>     

                        </h3>
                         <div id="checkout_login" class="collapse show" data-parent="#accordion" style="">
                            <div class="checkout_info">
                                <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing &amp; Shipping section.</p>  
                                <form action="{{ url('/user-login') }}" method="post"> 
                                    @csrf 
                                    <div class="form_group">
                                        <label>Username or email <span>*</span></label>
                                        <input type="text" name="username" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>">     
                                    </div>
                                    <div class="form_group">
                                        <label>Password  <span>*</span></label>
                                        <input type="password" name="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>">     
                                    </div> 
                                    <div class="form_group group_3 ">
                                        <button type="submit">Login</button>
                                        <label for="remember_box">
                                            <input id="remember_box" type="checkbox" name="remember_me" <?php if(isset($_COOKIE["username"])) { echo 'checked'; } ?>>
                                            <span> Remember me </span>
                                        </label>   
                                        <input type="hidden" name="page" value="checkout">  
                                    </div>
                                    <a href="#">Lost your password?</a>
                                </form>          
                            </div>
                        </div>    
                    </div>
                    
                </div>
            </div>
            @endif
            <div class="checkout_form">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <form action="{{ url('/place-order') }}" method="post">
                            @csrf 
                            <h3>Billing Details</h3>
                            <div class="row">

                                <div class="col-lg-12 mb-20">
                                    <label>Name <span>*</span></label>
                                    <input type="text" name="billing_name" value="<?php if($user) { echo $user->name; } ?>" required>
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Phone<span>*</span></label>
                                    <input type="text" name="billing_mobile" value="<?php if($user) { echo $user->mobile; } ?>" required>

                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label> Email Address <span>*</span></label>
                                    <input type="text" name="billing_email" value="<?php if($user) { echo $user->email; } ?>" required>

                                </div>
                                <div class="col-12 mb-20">
                                    <label for="country">Country <span>*</span></label>
                                    <select class="" name="billing_country" id="country" required>
                                    @foreach($countries as $row) 
                                        <option <?php if($user) { if($user->country) { if($user->country == $row->id) { echo 'selected'; }  } else { if($row->id==101) { echo 'selected'; }  }  } else { if($row->id==101) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-12 mb-20">
                                    <label for="country">State <span>*</span></label>
                                    <select class="selectize-select" name="billing_state" id="state" required>
                                    @foreach($states as $row) 
                                        <option <?php if($user) { if($user->state) { if($user->state == $row->id) { echo 'selected'; }  } else { if($row->id==35) { echo 'selected'; }  }  } else { if($row->id==35) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-12 mb-20">
                                    <label for="country">City <span>*</span></label>
                                    <select class="selectize-select"  name="billing_city" id="city" required>
                                    @foreach($cities as $row) 
                                        <option <?php if($user) { if($user->city) { if($user->city == $row->id) { echo 'selected'; }  } else { if($row->id==3992) { echo 'selected'; }  }  } else { if($row->id==3992) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="col-12 mb-20">
                                    <label>Street address <span>*</span></label>
                                    <input placeholder="House number and street name" type="text" name="billing_address1" value="<?php if($user) { if($user->address_1) { echo $user->address_1; } } ?>" required>
                                </div>
                                <div class="col-12 mb-20">
                                    <input placeholder="Apartment, suite, unit etc. (optional)" name="billing_address2" type="text" value="<?php if($user) { if($user->address_2) { echo $user->address_2; } } ?>">
                                </div>
                                <div class="col-lg-12 mb-20">
                                    <label>Pincode <span>*</span></label>
                                    <input type="text" placeholder="Pincode" name="billing_pincode" value="<?php if($user) { echo $user->pincode; } ?>" required>
                                </div>
                               

                                <div class="col-12 mb-20">
                                    <input id="address" type="checkbox" data-target="createp_account">
                                    <label class="righ_0 collapsed" for="address" data-bs-toggle="collapse" href="#collapsetwo" aria-controls="collapsetwo" aria-expanded="false">Ship to a different address?</label>

                                    <div id="collapsetwo" class="one collapse" data-parent="#accordion" style="">
                                        <div class="row">
                                            <div class="col-lg-12 mb-20">
                                                <label>Name <span>*</span></label>
                                                <input type="text" name="shipping_name" class="required">
                                            </div>
                                            <div class="col-lg-6 mb-20">
                                                <label>Phone<span>*</span></label>
                                                <input type="text" name="shipping_mobile" class="required">

                                            </div>
                                            <div class="col-lg-6">
                                                <label> Email Address <span>*</span></label>
                                                <input type="text" name="shipping_email" class="required">

                                            </div>
                                            <div class="col-12 mb-20">
                                                <label for="country">Country <span>*</span></label>
                                                <select class="required" name="shipping_country" id="country">
                                                @foreach($countries as $row) 
                                                    <option <?php if($user) { if($user->country) { if($user->country == $row->id) { echo 'selected'; }  } else { if($row->id==101) { echo 'selected'; }  }  } else { if($row->id==101) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 mb-20">
                                                <label for="country">State <span>*</span></label>
                                                <select class="required" name="shipping_state" id="state">
                                                @foreach($states as $row) 
                                                    <option <?php if($user) { if($user->state) { if($user->state == $row->id) { echo 'selected'; }  } else { if($row->id==35) { echo 'selected'; }  }  } else { if($row->id==35) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="col-12 mb-20">
                                                <label for="country">City <span>*</span></label>
                                                <select class="required"  name="shipping_city" id="city">
                                                @foreach($cities as $row) 
                                                    <option <?php if($user) { if($user->city) { if($user->city == $row->id) { echo 'selected'; }  } else { if($row->id==3992) { echo 'selected'; }  }  } else { if($row->id==3992) { echo 'selected'; }  }?> value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <label>Street address <span>*</span></label>
                                                <input placeholder="House number and street name" class="required" name="shipping_address1" type="text">
                                            </div>
                                            <div class="col-12 mb-20">
                                                <input placeholder="Apartment, suite, unit etc. (optional)" name="shipping_address2" type="text">
                                            </div>
                                            <div class="col-lg-12 mb-20">
                                                <label>Pincode <span>*</span></label>
                                                <input placeholder="Pincode" type="text" name="shipping_pincode" value="">
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="order-notes">
                                        <label for="order_note">Order Notes</label>
                                        <textarea id="order_note" name="notes" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                    </div>
                                </div>
                            </div>
                       
                    </div>
                    <div class="col-lg-6 col-md-6">
                       
                            <h3>Your order</h3>
                            <div class="order_table table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Product</th>
                                            <th>Weight</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $total = 0; $weight = 0; $dcharge=0; $transCharge=0; ?>
                                    @if(session('cart'))
                                        @foreach(session('cart') as $id => $details)
                                            <?php $total += $details['price'] * $details['qty'] ?>
                                            <?php $weight += $details['weight'] * $details['qty'] ?>
                                        <tr>
                                        <td><a href="{{url('product/'.$details['url'].'')}}"><img width="50" src="{{ url($details['image']) }}" alt="{{ $details['name'] }}"></a></td>
                                            <td>  <a href="{{url('product/'.$details['url'].'')}}">{{ $details['name'] }}</a> <strong> × {{ $details['qty'] }}</strong></td>
                                            <td> {{ $controller->readableMetric($details['qty'] * $details['weight']) }}</td>
                                            <td> {{ $details['qty'] * $details['price'] }}</td>
                                            
                                        </tr>
                                        @endforeach
                                    @endif
                                       
                                    </tbody>
                                    <tfoot id="carttotal">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Subtotal</th>
                                            <td>{{ $total }}</td>
                                        </tr>
                                        <div >
                                        <?php $tax = (18 / 100) * $total; if($user->State != 35 && $user->State != '') { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Tax IGST (18%)</th>
                                            <td> <?php echo $tax; ?></td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Tax SGST (9%)</th>
                                            <td> <?php echo (9 / 100) * $total; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Tax CGST (9%)</th>
                                            <td> <?php echo (9 / 100) * $total; ?></td>
                                        </tr>
                                       
                                        <?php } ?>
                                        <?php /* $dtype = Session::get('deliveryType'); $dcharge = $weight * 75; if($user->State == 35) { $dcharge = $weight * 100; } if($dcharge < 200 || $dtype == 'courier') { ?> 
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Courier Charges</th>
                                            <td>
                                                <?php echo $dcharge; ?>
                                            </td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Packing & Transport Charges</th>
                                            <td>
                                                <?php $dcharge = 200 + $weight * 5 ; echo $dcharge; ?>
                                            </td>
                                        </tr>
                                        <?php } */ $dcharge = 0; ?>
                                        <input type="hidden" name="dcharge" id="dcharge" value="<?php echo $dcharge; ?>">
                                        <input type="hidden" name="weight" id="weight" value="<?php echo $weight; ?>">
                                        <input type="hidden" name="total" id="total" value="<?php echo $total; ?>">
                                        <tr class="order_total">
                                            <td></td>
                                            <td></td>
                                            <th>Order Value</th>
                                            <td><strong>Rs. {{ $total+$tax+$dcharge }}</strong></td>
                                        </tr>
                                        </div>
                                    </tfoot>
                                </table>
                            </div>
                            <h3>Payment Method</h3>
                            <div class="payment_method">
                                <div class="panel-default">
                                    <input id="payment" value="C" name="payment_method" type="radio" checked data-target="createp_account">
                                    <label for="payment" data-bs-toggle="collapse" href="#method" aria-controls="method" class="collapsed" aria-expanded="false">Cash on delivery</label>

                                </div>
                                <div class="panel-default">
                                    <input id="payment_defult" value="O" name="payment_method" type="radio" data-target="createp_account">
                                    <label for="payment_defult" data-bs-toggle="collapse" href="#collapsedefult" aria-controls="collapsedefult" class="" aria-expanded="true">Online</label>
                                </div>
                                <div class="order_button">
                                    <button type="submit">Place Order</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="chargeModal" tabindex="-1" role="dialog" aria-labelledby="chargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white" id="chargeModalLabel">Transport Charge Alert</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p> Your Courier charge will be more than Rs. 500, would you like to take transport service to reduce your Courier charge
        <form method="post" action="{{ url('/choose-transport') }}">
        @csrf 
        <div class="form-group">
        <label>Would you like to choose transport service?</lebel>  
        <br>
        <div class="form-check form-check-inline">
        <input class="form-check-input transport-radio" type="radio" name="transportOption" id="inlineRadio1" value="Y" required>
        <label class="form-check-label" for="inlineRadio1">Yes</label>
        </div>
        <div class="form-check form-check-inline">
        <input class="form-check-input transport-radio" type="radio" name="transportOption" id="inlineRadio2" value="N">
        <label class="form-check-label" for="inlineRadio2">No</label>
        </div>
        </div>
        
        <div class="form-group" id="transportArea" style="display:none"> 
        <label for="exampleFormControlInput1">Enter your Transport Name</label>
        <input type="text" class="form-control" name="transportName" id="transportName" placeholder="Transport Name">
        </div>
        <div class="form-group text-right">
            <button type="submit" class="mt-2 btn btn-secondary">Submit</button>
        </div>
</form>
    </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


        @stop

        