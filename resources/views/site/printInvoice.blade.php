<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js" integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    #invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    /* padding: 15px; */
    border: 1px solid #aaa;
}

.invoice header {
    padding: 10px 10px;
    /* margin-bottom: 20px; */
    color: #fff;
    background: #0c8322;
    border-bottom: 1px solid #0c8322
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    padding: 10px;
}

.border-top {
    border-top: 1px solid #fff !important;
}
.invoice .invoice-to {
    text-align: left
}


.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 1.5rem;
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #0c8322
}

.invoice main {
    padding-bottom: 17px;
}

.invoice main .thanks {
    /* margin-top: -100px; */
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #0c8322;
    padding:10px;
}



.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th ,.invoice table tfoot td {
    font-size:13px;
    padding: 5px;
    background: #eee;
    border-bottom: 1px solid #fff
}
.white {
    background:#fff !important;
}
.invoice table td {
    border-bottom: 0
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #0c8322;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: center;
    /* font-size: 1.2em */
}

.invoice table .no {
    color: #fff;
    /* font-size: 1.2em; */
    background: #0c8322
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #0c8322;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}


.invoice table tfoot tr td {
    border: none
}
.h2, h2 {
    font-size: 1.5rem;
}

.invoice footer {
    width: 100%;
    text-align: center;
    background: #0c8322;
    color: #fff;
    border-top: 1px solid #aaa;
    padding: 8px 0
}
body {
    font-size:13px !important; 
}
@media print {
    /* .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    } */

    /* .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    } */

    /* .invoice>div:last-child {
        page-break-before: always
    } */
}

</style>
<!--Author      : @arboshiki-->
<div id="invoice">

   
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <h2 class="name">
                            GLANZ SPECIAL TOOLS
                        </h2>
                        <div class="address">No.1/178, Vadakku Thottam,Near Nagasakthi Amman Beedam, </div>
                        <div class="address"> Malumichampatti, Coimbatore - 641021</div>
                        
                        <div class="address">GSTIN NO : 33DZPPS5510C1ZE</div>
                        <div class="address">State : Tamilnadu, Code : 33	</div>
                      
                    </div>
                    <div class="col">
                       
                    </div>
                    <div class="col company-details">
                    <div>Original for Receipt</div>
                       
                       
                        <div class="address"><i class="fa fa-phone"></i> +91 90036 79961</div>
                        <div class="email"><i class="fa fa-envelope"></i> glanzspecialtools@gmail.com</div>
                        <div class="email"><i class="fa fa-globe"></i> www.glanzspecialtools.com</div>
                        
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                      <h2 class="to" >Bill To</h2>
                      <div class="address">{{ $shipping->BillingName }} </div>
                        <div class="address">{{ $shipping->BillingAddress1 }}
                        <br>
                        {{ $shipping->BillingAddress2 }}</div>
                        <div class="address">
                        {{ $shipping->BillingEmail }} 
                        </div>
                        <div class="address">
                        {{ $shipping->BillingMobile }} 
                        </div>
                        
                        <div class="address">
                        {{ $controller->getStateName($shipping->BillingState) }} 
                        </div>
                        <div class="address">
                        {{ $controller->getCityName($shipping->BillingCity) }}
                        </div>
                        				
                    </div>
                    <div class="col invoice-to">
                        <h2 class="to">Supply to:</h2>
                        <div class="address">{{ $shipping->ShippingName }} </div>
                        <div class="address">{{ $shipping->ShippingAddress1 }}
                        <br>
                        {{ $shipping->ShippingAddress2 }}</div>
                        <div class="address">
                        {{ $shipping->ShippingEmail }} 
                        </div>
                        <div class="address">
                        {{ $shipping->ShippingMobile }} 
                        </div>
                        
                        <div class="address">
                        {{ $controller->getStateName($shipping->ShippingState) }} 
                        </div>
                        <div class="address">
                        {{ $controller->getCityName($shipping->ShippingCity) }}
                        </div>
                        
                        				
                    </div>
                    <div class="col invoice-details">
                    <h2 class="name">
                           
                           INVOICE {{ $order->OrderNo }}
                       
                   </h2>
                        <div class="date">Date: {{ date('d-M-Y',strtotime($order->created_at)) }}   </div>
                   
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="width: 20px;">#</th>
                            <th class="text-left">DESCRIPTION</th>
                            <th class="text-center" style="width: 120px;">HSN CODE</th>
                            <th class="text-center" style="width: 120px;">QTY</th>
                            <th class="text-center" style="width: 120px;">UNITS</th>
                            <th class="text-center" style="width: 120px;">RATE</th>
                            <th class="text-center" style="width: 120px;">DISCOUNT</th>
                            <th class="text-right" style="width: 120px;">AMOUNT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; $sub=0;?>
                    @foreach($orders as $row)
                        <tr>
                            <?php $sub = $sub + $row->Total; ?>
                            <td class="no"><?php echo $i; ?></td>
                            <td class="text-left">
                                
                                {{ $row->ProductName }}
                               
                                
                               
                            </td>
                            <td class="unit">8207</td>
                            <td class="qty">{{ $row->Qty }}</td>
                            <td class="qty">PCS</td>
                            <td class="qty">{{ number_format($row->Price,2) }}</td>
                            <td class="qty">{{ $row->Discount }}%</td>
                            <td class="total text-right">{{ number_format($row->Total,2) }}</td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                    <tr>
                        <td class="no"></td>
                        <td></td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total"></td>
                    </tr>
                    <tr>
                        <td class="no"></td>
                        <td></td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total"></td>
                    </tr>
                    <tr>
                        <td class="no"></td>
                        <td></td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total"></td>
                    </tr>
                    <tr>
                        <td class="no"></td>
                        <td></td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total"></td>
                    </tr>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            
                            <td class="no"></td>
                            <td class="text-left">Taxable Value</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="total text-right">{{ number_format($sub,2) }} </td>
                        </tr>
                        <?php if($user->State != 35 && $user->State != '') { ?>
                        <tr>
                            
                            <td class="no"></td>
                            <td class="text-left">Tax IGST 18%</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> 18 % </td>
                            <td class="total text-right">{{ number_format($order->Tax,2) }} </td>
                        </tr>
                        <?php } else { ?> 
                        <tr>
                            
                            <td class="no"></td>
                            <td class="text-left">Tax CGST</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> 9 % </td>
                            <td class="total text-right">{{ number_format((9 / 100) * $order->Total,2) }} </td>
                        </tr>

                        <tr>
                            
                            <td class="no"></td>
                            <td class="text-left">Tax SGST</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> 9 % </td>
                            <td class="total text-right">{{ number_format((9 / 100) * $order->Total,2) }} </td>
                        </tr>

                        <?php } ?>
                        <?php /* if($order->CourierCharges) { ?> 
                        <tr>
                            <td class="no"></td>
                            <td class="text-left">Courier Charge</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> </td>
                            <td class="total text-right">{{ number_format($order->CourierCharges,2) }} </td>
                        </tr>
                        <?php } else { ?> 
                            <tr>
                            <td class="no"></td>
                            <td class="text-left">Packing & Transport Charge</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> </td>
                            <td class="total text-right">{{ number_format($order->PackingTransportCharges,2) }} </td>
                        </tr>
                        <?php } */?>
                        <tr>
                            
                            <td class="no"></td>
                            <td class="text-left">Rounded Off</td>
                            <td class="unit"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center"> </td>
                            <td class="total text-right"> <?php $round = round($order->GrandTotal); ?>{{ number_format($round - $order->GrandTotal,2) }}</td>
                        </tr>
                        <tr>
                        <td class="no"></td>
                        <td></td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total"></td>
                    </tr>
                    <tr>
                        <td class="no"></td>
                        <td class="text-left">Total</td>
                        <td class="unit"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="qty"></td>
                        <td class="total text-right">{{ number_format($round,2) }}</td>
                    </tr>
                        <tr class="white">
                            <td colspan="6" class="border-top white">Amount (In words) : {{ $controller->convert_number($round)}} Only</td>
                            <td colspan="2" class="border-top white">For Glanz Special Tools</td>
                        </tr>
                        <tr>
                            <td colspan="2"  class="white"></td>
                            <td colspan="4"  class="white"></td>
                            <td colspan="2"  class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2"  class="white"></td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="white"></td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="white"></td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="white"></td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="white"></td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="white">Customer's Seal and Signature</td>
                            <td colspan="4" class="white"></td>
                            <td colspan="2" class="white">Authorised Signatory</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="notices">
                    <div>Declaration:</div>
                    <div class="notice">We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</div>
                </div>
            </main>
            <footer>
               
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>