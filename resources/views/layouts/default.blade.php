

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Glanz Special Tools | Automobile Special Tools</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

     <!-- CSS 
    ========================= -->
     <!--bootstrap min css-->
     
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <!--owl carousel min css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.carousel.min.css') }}">
    <!--slick min css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/slick.css') }}">
    <!--magnific popup min css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/magnific-popup.css') }}">
    <!--font awesome css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font.awesome.css') }}">
    <!--ionicons min css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/ionicons.min.css') }}">
    <!--animate css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}">
    <!--jquery ui min css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery-ui.min.css') }}">
    <!--slinky menu css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/slinky.menu.css') }}">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins.css') }}">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css?ver=2') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">

    
    <!--modernizr min js here-->
    <script src="{{ URL::asset('assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--popper min js-->
    <script src="{{ URL::asset('assets/js/popper.js') }}"></script>
    <!--bootstrap min js-->
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>

</head>

<body>

    <!-- Main Wrapper Start -->
    @include('includes.header')

    @yield('content')
    
    @include('includes.footer')

<!-- JS
============================================ -->
<!--jquery min js-->



<!--owl carousel min js-->
<script src="{{ URL::asset('assets/js/owl.carousel.min.js') }}"></script>
<!--slick min js-->
<script src="{{ URL::asset('assets/js/slick.min.js') }}"></script>
<!--magnific popup min js-->
<script src="{{ URL::asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<!--jquery countdown min js-->
<script src="{{ URL::asset('assets/js/jquery.countdown.js') }}"></script>
<!--jquery ui min js-->
<script src="{{ URL::asset('assets/js/jquery.ui.js') }}"></script>
<!--jquery elevatezoom min js-->
<script src="{{ URL::asset('assets/js/jquery.elevatezoom.js') }}"></script>
<!--isotope packaged min js-->
<script src="{{ URL::asset('assets/js/isotope.pkgd.min.js') }}"></script>
<!--slinky menu js-->
<script src="{{ URL::asset('assets/js/slinky.menu.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ URL::asset('assets/js/plugins.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<!-- Main JS -->
<script src="{{ URL::asset('assets/js/main.js?ver=1') }}"></script>
<script src="{{ URL::asset('assets/js/custom.js?ver=rand()') }}"></script>


<script type="text/javascript">

function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>
<style>
    #google_translate_element {
  color: transparent;
}
#google_translate_element a {
  display: none;
}
div.goog-te-gadget {
  color: transparent !important;
}

.goog-te-combo {
    padding: 4px;
    background: #7c0c83;
    color: #fff;
}
.goog-te-gadget {
    position: absolute;
    z-index:999;
    top: -23px;
}
.goog-te-banner-frame.skiptranslate {
display: none !important;
} 
</style>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</body>

</html>
    