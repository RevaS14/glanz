$( document ).ready(function() {

   $('#dataTable').DataTable({
    "ordering": false
   });

   $(".transport-radio").change(function(){
    var val = $(this).val();
        if(val=='Y') {
            $('#transportArea').show();
            $("#transportName").prop('required',true);
        } else {
            $('#transportArea').hide();
            $("#transportName").prop('required',false);
        }
   });

   base_url = $('#base_url').val();
   token = $('#csrf_token').val();
   page = $('#page').val();

   $('.quick_view').click(function(e){
       e.preventDefault();
       id = $(this).attr('id');
      
        $.ajax({
            /* the route pointing to the post function */
            url: '/quick-view',
            type: 'POST',
            data: {productid:id,_token: token},
            dataType: 'html',
            success: function (data) { 
                $("#quick_view_area").html(data); 
                    /*---product navactive activation---*/
                $('.product_navactive').owlCarousel({
                    autoplay: true,
                    loop: false,
                    nav: true,
                    autoplay: false,
                    autoplayTimeout: 8000,
                    items: 4,
                    dots:false,
                    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                    responsiveClass:true,
                    responsive:{
                            0:{
                            items:1,
                        },
                        250:{
                            items:2,
                        },
                        480:{
                            items:3,
                        },
                        768:{
                            items:4,
                        },
                    
                    }
                });
                $('.product_navactive a').on('click',function(e){
                    e.preventDefault();
              
                    var $href = $(this).attr('href');
              
                    $('.product_navactive a').removeClass('active');
                    $(this).addClass('active');
              
                    $('.product-details-large .tab-pane').removeClass('active show');
                    $('.product-details-large '+ $href ).addClass('active show');
              
                  })
                $("#quick_view_box").modal('show');
            }
        }); 

   });

   $(document).on('click', '.cart-btn', function(e){
    e.preventDefault();
    id = $(this).attr('data-id');
    product_name = $(this).attr('data-name');
    image = $(this).attr('data-image');
    price = $(this).attr('data-price');
    link = $(this).attr('data-link');
    qty = $(this).attr('data-qty');
    weight = $(this).attr('data-weight');
    token = $('#csrf_token').val();
     $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-cart',
         type: 'POST',
         data: {id:id,_token: token,name:product_name,image:image,price:price,qty:qty,link:link,weight:weight},
         dataType: 'json',
         success: function (data) { 
            $('.mini_cart').html(data.page);
            $('.cart_quantity').html(data.count);
            $('.success-toast .toast-body').html('Product Added to cart..');
            $('.success-toast').toast('show');
         }
     }); 

    });


    $(document).on('click', '.wishlist_button', function(e){
        e.preventDefault();
        id = $(this).attr('id');
        elem = $(this);
        token = $('#csrf_token').val();
         $.ajax({
             /* the route pointing to the post function */
             url: '/add-to-wishlist',
             type: 'POST',
             data: {id:id,_token: token},
             dataType: 'json',
             success: function (data) { 
                 if(data=='guest') {
                    window.location.href = 'http://127.0.0.1:8000/login';
                 } else {
                    elem.removeClass( "wishlist_button");
                    elem.addClass( "wishlist_active");
                    $('.wishlist_quantity').html(data.count);
                    $('.success-toast .toast-body').html('Product Added to wish list..');
                    $('.success-toast').toast('show');

                 }
             }
         }); 
    
        });

    $(document).on('click', '.cart_remove a', function(e){
        e.preventDefault();

        id = $(this).attr('data-id');
        token = $('#csrf_token').val();
        
        element = $(this);

        $.ajax({
            /* the route pointing to the post function */
            url: '/remove-from-cart',
            type: 'POST',
            data: {id:id,_token: token},
            dataType: 'json',
            success: function (data) { 
                if(data.count <= 0) {
                    if(page=='http://127.0.0.1:8000/cart') {
                        location.reload();
                    }
                }
               $('.mini_cart').html(data.page);
               $('.cart_quantity').html(data.count);
               $('.shopping_cart_area').html(data.cart);
               element.closest('tr').remove();
            }
        }); 
    });

    $(document).on('keyup mouseup', '.qty', function(e){
        $(this).closest('.add_to_cart_area').find('.cart-btn').attr('data-qty',$(this).val());
        qty = $(this).val();
        cart_price = $(this).closest('tr').find('.product-price').attr('data-price');
        discount = 0;
        if ($(this).parent().closest('.single_product').find(".label_product")[0]){
            discount = $(this).parent().closest('.single_product').find(".label_sale").attr('data-discount');
        }
        if(discount==0) {
            price = $(this).parent().closest('.single_product').find(".regular_price").attr('data-price');
            $(this).parent().closest('.single_product').find('.regular_price').html('Rs. '+ qty*price);
        } else {
            price = $(this).parent().closest('.single_product').find(".current_price").attr('data-price');
            old_price = $(this).parent().closest('.single_product').find(".old_price").attr('data-price');
            total = qty*old_price;
            $(this).parent().closest('.single_product').find('.current_price').html('Rs. '+ qty*price);
            $(this).parent().closest('.single_product').find('.old_price').html('Rs. '+ qty*old_price);
        }       
        $(this).closest('tr').find('.product_total').html('Rs. '+qty*cart_price);
        if(page=='http://127.0.0.1:8000/cart') {
            id = $(this).attr('data-id');
            $.ajax({
                /* the route pointing to the post function */
                url: '/update-cart',
                type: 'POST',
                data: {id:id,_token: token,qty:qty},
                dataType: 'json',
                success: function (data) { 
                $('.mini_cart').html(data.page);
                $('.cart_quantity').html(data.count);
                $('.shopping_cart_area').html(data.cart);
                }
            }); 
        }
        
    });

    $('#country').change(function(e){
        id = $(this).val();
        $.ajax({
            /* the route pointing to the post function */
            url: '/get-states',
            type: 'POST',
            data: {id:id,_token: token},
            dataType: 'json',
            success: function (data) { 
                     $('#state')
                     .empty();
                $.each(data, function(k, v) {
                    $('#state')
                    .append('<option value="'+v.id+'">'+v.name+'</option>');
                });
                
            }
        }); 
    });

    $('#state').change(function(e){
        id = $(this).val();
        $.ajax({
            /* the route pointing to the post function */
            url: '/get-cities',
            type: 'POST',
            data: {id:id,_token: token},
            dataType: 'json',
            success: function (data) { 
                     $('#city')
                     .empty();
                $.each(data, function(k, v) {
                    $('#city')
                    .append('<option value="'+v.id+'">'+v.name+'</option>');
                });
            }
        }); 
        total = $('#total').val();
        weight = $('#weight').val();
        $.ajax({
            /* the route pointing to the post function */
            url: '/get-cart-total',
            type: 'POST',
            data: {id:id,_token: token,total:total,weight:weight},
            dataType: 'html',
            success: function (data) { 
               $('#carttotal').html(data);
            }
        }); 
    });

    $("#address").change(function() {
        if(this.checked) {
            $(".required").prop('required',true);
        } else {
            $(".required").prop('required',false);
        }
    });

    
    
});
