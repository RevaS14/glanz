<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Request;
use App\Models\Category;
use App\Models\Slider;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Order;
use App\Models\Newsletter;
use App\Models\OrderDetail;
use App\Models\ShippingDetail;
use App\Models\Wisthlist;
use App\Models\Rating;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index() {
       
        $categories = Category::getCategories();
        $sliders = Slider::getSliders();
        $homeProducts = Product::getFeaturedProducts();
        $offeredProducts = Product::getOfferedProducts();
        $mostViewdProducts = Product::getmostViewdProducts();
        $data = array(
            "categories" => $categories,
            "sliders" => $sliders,
            "homeProducts" => $homeProducts,
            "offeredProducts" => $offeredProducts,
            "mostViewdProducts" => $mostViewdProducts,
        );
        return view('site.home')->with($data);
    }

    public function getProductSingleImage($id) {
        $singleImage =  ProductImage::getProductSingleImage($id);
        return $singleImage->Image;
    }

    public function getFeatured($product) {
        return view('site.featured')->with("product",$product);
    }

    public function getMostViewed($product) {
        return view('site.mostviewed')->with(array("product"=>$product,'controller'=>$this));
    }

    public function getCart() {
        return view('site.cart')->with(array('controller'=>$this));
    }

    public function quickView(Request $request) {
        $request = Request::instance();
        $productid = $request->input('productid');
        $product =  Product::getSingleProduct($productid);
        $productImages =  ProductImage::getProductImages($productid);
        return view('site.productmodel')->with(array("product"=>$product,"images"=>$productImages, 'controller'=>$this));
    }

    public function getSingleProduct($id){
        return Product::getSingleProduct($id);
    }

    public function getUser($id){
        return User::find($id);
    }

    public function updateCart(Request $request) {
        $request = Request::instance();
        $id = $request->input('id');
        $qty = $request->input('qty');
        $cart = session()->get('cart');
        $cart[$id]['qty'] = $qty;
        session()->put('cart', $cart);
        $data = array(
            "page" => (string)view('site.cart')->with(array('controller'=>$this)),
            "cart" => (string)view('site.rendercart')->with(array('controller'=>$this)),
            "count" => count($cart)
        );
        return json_encode($data);
       
    }

    public function getWishlistCount() {
        $user_id = Session::get('user_id');
        if($user_id) {
            $cart = Wisthlist::select('*')
            ->where("UserId", "=", $user_id)
            ->orderBy('id', 'DESC')->get();
            if(count($cart)>0) {
                echo count($cart);
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function addToCart(Request $request) {
        $request = Request::instance();
        $id = $request->input('id');
        $name = $request->input('name');
        $qty = $request->input('qty');
        $price = $request->input('price');
        $image = $request->input('image');
        $url = $request->input('url');
        $weight = $request->input('weight');
        $cart = session()->get('cart');
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                    $id => [
                        "name" => $name,
                        "qty" => $qty,
                        "price" => $price,
                        "url" => $url,
                        "image" => $image,
                        "weight" => $weight
                    ]
            ];
        } else {
            if(isset($cart[$id])) {
                $cart[$id]['qty']++;
            } else{
                $cart[$id] = [
                    "name" => $name,
                    "qty" => $qty,
                    "price" => $price,
                    "url" => $url,
                    "image" => $image,
                    "weight" => $weight
                ];
            }
            
        }
        session()->put('cart', $cart);

       $data = array(
           "page" => (string)view('site.cart')->with(array('controller'=>$this)),
           "count" => count($cart)
       );
       return json_encode($data);
    }

    public function checkWishList($id) {
        $user_id = Session::get('user_id');
        if($user_id) {
            $cart = Wisthlist::select('*')
            ->where("UserId", "=", $user_id)
            ->where("ProductId", "=", $id)->first();
            if($cart) {
                return 'wishlist_active';
            } else {
                return 'wishlist_button';
            }
        } else {
            return 'wishlist_button';
        }
    }

    public function addToWishlist(Request $request) {
        $request = Request::instance();
        $id = $request->input('id');
        $user_id = Session::get('user_id');
        if($user_id) {

            Wisthlist::create([
                'UserId' => $user_id,
                'ProductId' => $id
            ]);

            $cart = Wisthlist::select('*')
            ->where("UserId", "=", $user_id)
            ->orderBy('id', 'DESC')->get();
    
            $data = array(
                "count" => count($cart)
            );

            return json_encode($data);
        } else {
            Session::flash('error', 'Please login to add product to wish list!'); 
            $data = 'guest';
        }
        return json_encode($data);
    }

    public function readableMetric($mass)
    {
        $units = [
            -3 => "Tonne",
             0 => "Kg",
             3 => "Grams",
             6 => "Mg",
        ];
    
        foreach ($units as $x => $unit) {
            if ( ($newMass = $mass * 10 ** $x)  >= 1 ) {
                return "{$newMass} {$unit}";
            }
        }
    }

    public function remove(Request $request)
    {
        $request = Request::instance();
        $cart = session()->get('cart');
        if($request->input('id')) {
            $cart = session()->get('cart');
            if(isset($cart[$request->input('id')])) {
                unset($cart[$request->input('id')]);
                session()->put('cart', $cart);
            }
        }
        $data = array(
            "page" => (string)view('site.cart')->with(array('controller'=>$this)),
            "cart" => (string)view('site.rendercart')->with(array('controller'=>$this)),
            "count" => count($cart)
        );
        Session::put('deliveryType', '');
        Session::put('transportName', '');
        return json_encode($data);
    }

    public function removeWishlist($id)
    {
        $user=Wisthlist::find($id);
        $user->delete(); //returns true/false
        Session::flash('success', 'Product removed successfully!'); 
        return redirect('/wishlist');
    }

    public function clearCart()
    {
            $cart = session()->get('cart');
            session(['cart' => '']);
            Session::put('deliveryType', '');
            Session::put('transportName', '');
            unset($cart);
    }

    public function category($link) {
        $category = Category::getCategoryByLink($link);
        $categories = Category::getCategories();
        $products = Category::getProducts($category->id);
        $data = array(
            "category" => $category,
            "categories" => $categories,
            'controller'=>$this,
            'products'=>$products
        );
        
        if(!$category) {
            return redirect('/404');
        }
        
        return view('site.category')->with($data);
    }

    public function search(Request $request) {
        $request = Request::instance();
        $keyword = $request->input('keyword');
        $categories = Category::getCategories();
        $products = Product::searchProducts($keyword);
        $data = array(
            "categories" => $categories,
            'controller'=>$this,
            'products'=>$products
        );
        
        if(!$keyword) {
            return redirect('/404');
        }
        
        return view('site.search')->with($data);
    }
    public function subscribe(Request $request) {
        $request = Request::instance();
        $keyword = $request->input('email');
        $ex = Newsletter::select("*")->where("email", "=", $keyword)->get();
        if(count($ex)) {
            return redirect('/#newsletter')->with('sub_error', 'Already subscribed!');
        }
        $post = new Newsletter();
        $post->email = $keyword;
        $post->save();
        return redirect('/#newsletter')->with('message', 'Thank you! Subscribed successfully!');
        
    }

    public function overall_rating($id) {
        $ratings = Rating::select("*")->where("ProductId", "=", $id)->where("Status", "=", 'A')->get();
        $users_rated = count($ratings);
        $overall_rating =0;
        if($users_rated) {
            $sum_of_rating = 0;
            foreach($ratings as $row) {
                $sum_of_rating += $row->Star;
            }
            $sum_of_max_rating_of_user_count = $users_rated * 5;
            $overall_rating = ($sum_of_rating * 5) / $sum_of_max_rating_of_user_count;
        } 
        return $overall_rating;
    }


    public function product($link) {
        $product = Product::getProductByLink($link);
        if(!$product) {
            return redirect('/404');
        }
        $ratings = Rating::select("*")->where("ProductId", "=", $product->id)->where("Status", "=", 'A')->get();
        $users_rated = count($ratings);
        $overall_rating =0;
        if($users_rated) {
            $sum_of_rating = 0;
            foreach($ratings as $row) {
                $sum_of_rating += $row->Star;
            }
            $sum_of_max_rating_of_user_count = $users_rated * 5;
            $overall_rating = ($sum_of_rating * 5) / $sum_of_max_rating_of_user_count;
        }
        $categories = Category::getCategories();
        $category = Category::getProductCategory($product->id);
        $productImages = ProductImage::getProductImages($product->id);
        $relatedProducts = Product::getRelatedProducts($category->id);
        $data = array(
            "productImages" => $productImages,
            "overall_rating" => $overall_rating,
            "categories" => $categories,
            'controller'=>$this,
            'product'=>$product,
            'category'=>$category,
            'relatedProducts'=>$relatedProducts,
            'ratings' => $ratings
        );

        return view('site.product')->with($data);
    }

    public function login() {
        $request = Request::instance();
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this,
            "request" => $request
        );
        return view('site.login')->with($data);
    }

    public function review(Request $request) {
        $user_id = Session::get('user_id');
        if($user_id) {
            $user = User::find($user_id);
        } else {
            $user = '';
            Session::flash('error', 'Please login or create account to review our products..!'); 
            return redirect('/login');
        }
        $request = Request::instance();
        $star = $request->input('star');
        $review = $request->input('review');
        $productId = $request->input('productId');

        $exists = Rating::select('*')
        ->where("UserId", "=", $user_id)
        ->where("ProductId", "=", $productId)->first();

        $product = $this->getSingleProduct($productId);

        if($exists) {
            Session::flash('error', 'Already review submitted for this product!'); 
            return redirect('/product/'.$product->ProductLink);
        }

        $rating = new Rating;
        $rating->UserId = $user_id;
        $rating->ProductId = $productId;
        $rating->Star = $star;
        $rating->Review = $review;
        $rating->save();
        Session::flash('success', 'Review submitted successfully! Please wait for admin approval.'); 
        return redirect('/product/'.$product->ProductLink);
    }

    public function userLogin(Request $request) {
        $request = Request::instance();
        $username = $request->input('username');
        $page = Session::get('page');
        $user =  User::select("*")
        ->where("mobile", "=", $request->input('username'))
        ->orWhere("email", "=", $request->input('username'))->first();
        if($user){
            $remember_me = $request->exists('remember_me') ? true : false;
            $fieldType = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';
            if(Auth::guard()->attempt(array($fieldType => $request->input('username'), 'password' => $request->input('password')),$remember_me))
            {
                // Authentication passed...
                Session::put('user_id', $user->id);
                Session::put('user_name', $user->name);
                if($remember_me) {
                    setcookie ("username", $username, time()+ (10 * 365 * 24 * 60 * 60));  
                    setcookie ("password", $request->input('password'),  time()+ (10 * 365 * 24 * 60 * 60));
                }
                if($page=='checkout') {
                    return redirect('/checkout');
                } else {
                return redirect('/');
                }
            } else {
                Session::flash('error', 'Invalid Credentials!'); 
                return redirect('/login');
            }
        } 
    }

    public function register(Request $request) {
        $request = Request::instance();
        $mobile = $request->input('mobile');
        $email = $request->input('email');

        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );


        $isExistsEmail =  User::select("*")->where("email", "=", $request->input('email'))->first();
        $isExistsMobile =  User::select("*")->where("email", "=", $request->input('mobile'))->first();
        if($isExistsEmail) {
            Session::flash('error', 'Email Already Exists!'); 
            return redirect('/login');
        }
        if($isExistsMobile) {
            Session::flash('error', 'Mobile Number Already Exists!'); 
            return redirect('/login');
        }

        $file = User::create([
            'name' => $request->input('username'),
            'email' => $email,
            'mobile' => $mobile,
            'password' => Hash::make($request->input('password'))
        ]);

        Session::flash('success', 'Registered Successfully..Please login to your account!'); 
        return redirect('/login');

        return view('site.login')->with($data);
    }

    public function getStates(Request $request) {
        $request = Request::instance();
        $id = $request->input('id');
        $states = State::select("*")->where("country_id", "=", $id)->get();
        return json_encode($states);
    }

    public function getCartTotal(Request $request) {
        $request = Request::instance();
        $data = array(
            "state" => $request->input('id'),
            "weight" => $request->input('weight'),
            "total" => $request->input('total')
        );
        return view('site._tfoot')->with($data);
    }

    public function getCities(Request $request) {
        $request = Request::instance();
        $id = $request->input('id');
        $states = City::select("*")->where("state_id", "=", $id)->get();
        return json_encode($states);
    }

    public function cart() {
        $cart = session()->get('cart');
        if(!$cart) {
            return redirect('/');
        }
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.maincart')->with($data);
    }

    public function wishlist() {
        $user_id = Session::get('user_id');
        if($user_id) {
            $user = User::find($user_id);
        } else {
            $user = '';
            Session::flash('error', 'Please login or create account to wislist our products..!'); 
            return redirect('/login');
        }
        $cart = Wisthlist::select('*')
        ->where("UserId", "=", $user_id)
        ->orderBy('id', 'DESC')->get();
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            "list" => $cart,
            'controller'=>$this
        );
        return view('site.wishlist')->with($data);
    }

    public function checkout() {
        $cart = session()->get('cart');
        if(!$cart) {
            return redirect('/');
        }
        $user_id = Session::get('user_id');
        $deliveryType = Session::get('deliveryType');
        if($user_id) {
            $user = User::find($user_id);
        } else {
            Session::put('page', 'checkout');
            $user = '';
            Session::flash('error', 'Please login or create account to order our products..!'); 
            return redirect('/login');
        }
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            "user" => $user,
            "countries" => Country::all(),
            "states" => State::select("*")->where("country_id", "=", 101)->get(),
            "cities" => City::select("*")->where("state_id", "=", 35)->get(),
            'controller'=>$this,
            'deliveryType'=>$deliveryType
        );
        return view('site.checkout')->with($data);
    }

    public function chooseTransport(Request $request) {
        $request = Request::instance();
        $transportName = $request->input('transportName');
        $transportOption = $request->input('transportOption');
        if($transportOption == 'Y') {
        Session::put('deliveryType', 'truck');
        Session::put('transportName', $transportName);
        } else {
            Session::put('deliveryType', 'courier');
            Session::put('transportName', '');
        }
        return redirect('/checkout');
    }

    public function placeOrder(Request $request) {
        $request = Request::instance();
        $dtype = Session::get('deliveryType');
        $transportName = Session::get('transportName');
        $billing_name = $request->input('billing_name');
        $billing_email =  $request->input('billing_email');
        $billing_mobile = $request->input('billing_mobile');
        $billing_country =  $request->input('billing_country');
        $billing_state =  $request->input('billing_state');
        $billing_city =  $request->input('billing_city');
        $billing_address1 =  $request->input('billing_address1');
        $billing_address2 =  $request->input('billing_address2');
        $billing_pincode =  $request->input('billing_pincode');
        $dcharge =  $request->input('dcharge');
        

        $shipping_name = $request->input('shipping_name');
        $shipping_email =  $request->input('shipping_email');
        $shipping_mobile = $request->input('shipping_mobile');
        $shipping_country =  $request->input('shipping_country');
        $shipping_state =  $request->input('shipping_state');
        $shipping_city =  $request->input('shipping_city');
        $shipping_address1 =  $request->input('shipping_address1');
        $shipping_address2 =  $request->input('shipping_address2');
        $shipping_pincode =  $request->input('shipping_pincode');
        $payment_method =  $request->input('payment_method');

        $notes =  $request->input('notes');
        
        $user_id = Session::get('user_id');
        $cart = session()->get('cart');
        $total = 0;
        foreach ($cart as $id => $item) {
            $total = $total + $item['qty'] * $item['price'];
        }

        $tax = (18 / 100) * $total;
        $delivery_charge = 50;
       
        $order = new Order;
        $order->UserId = $user_id;
        $order->Notes = $notes;
        $order->Total = $total;
        //$order->DeliveryCharge = $delivery_charge;
        $order->Tax = $tax;
       
        $order->PaymentMethod = $payment_method;
        if($dtype=='courier') {
            $order->CourierCharges = $dcharge;
        } else {
            $order->PackingTransportCharges = $dcharge;
        }
        if($transportName) {
            $order->TransportName = $transportName;
        }
        $order->GrandTotal = $total+$tax+$dcharge;
        $order->save();
        $orderid = $order->id;

        $shipping = new ShippingDetail;
        $shipping->UserId = $user_id;
        $shipping->OrderId = $id;
        $shipping->BillingName = $billing_name;
        $shipping->BillingEmail = $billing_email;
        $shipping->BillingMobile = $billing_mobile;
        $shipping->BillingCountry = $billing_country;
        $shipping->BillingState = $billing_state;
        $shipping->BillingCity = $billing_city;
        $shipping->BillingAddress1 = $billing_address1;
        $shipping->BillingAddress2 = $billing_address2;
        $shipping->BillingPincode = $billing_pincode;
        
        if($shipping_name) {
            $shipping->ShippingName = $shipping_name;
            $shipping->ShippingEmail = $shipping_email;
            $shipping->ShippingMobile = $shipping_mobile;
            $shipping->ShippingCountry = $shipping_country;
            $shipping->ShippingState = $shipping_state;
            $shipping->ShippingCity = $shipping_city;
            $shipping->ShippingAddress1 = $shipping_address1;
            $shipping->ShippingAddress2 = $shipping_address2;
            $shipping->ShippingPincode = $shipping_pincode;
        } else {
            $shipping->ShippingName = $billing_name;
            $shipping->ShippingEmail = $billing_email;
            $shipping->ShippingMobile = $billing_mobile;
            $shipping->ShippingCountry = $billing_country;
            $shipping->ShippingState = $billing_state;
            $shipping->ShippingCity = $billing_city;
            $shipping->ShippingAddress1 = $billing_address1;
            $shipping->ShippingAddress2 = $billing_address2;
            $shipping->ShippingPincode = $billing_pincode;
        }
        $shipping->save();
        $shipping_id = $shipping->id;
        foreach ($cart as $id => $item) {
            $details = new OrderDetail;
            $details->UserId = $user_id;
            $details->OrderId = $orderid;
            $details->ProductId = $id;
            $details->Qty = $item['qty'];
            $details->Price = $item['price'];
            $details->Total = $item['qty'] * $item['price'];
            $details->save();
        } 
        $orderNo = 'GZ-'.$orderid;
        $single_order = Order::findOrFail($orderid);
        $single_order->ShippingId = $shipping_id;
        $single_order->OrderNo = $orderNo;
        $single_order->save();
        Session::forget('cart');
        Session::put('deliveryType', '');
        Session::put('transportName', '');
        // $this->sendMail($email,$id);
        return redirect('/success');
    }

    public function editAccount(Request $request) {
        $request = Request::instance();
        $mobile = $request->input('mobile');
        $name = $request->input('name');
        $country = $request->input('country');
        $state = $request->input('state');
        $city = $request->input('city');
        $address1 = $request->input('address1');
        $address2 = $request->input('address2');

        $pincode = $request->input('pincode');

        $post = User::find(Session::get('user_id'));
        $post->name = $name;
        $post->mobile = $mobile;
        $post->country = $country;
        $post->state = $state;
        $post->city = $city;
        $post->pincode = $pincode;
        $post->address_1 = $address1;
        if($address2) {
            $post->address_2 = $address2;
        }
        $post->save();
        
        Session::flash('success', 'Account Updated Successfully!'); 
        return redirect('/my-account');

    }

    public function changePassword(Request $request) {
        $request = Request::instance();
        $cpassword = $request->input('cpassword');
        $npassword = $request->input('npassword');
        $confirmpassword = $request->input('confirmpassword');
        $pass = Hash::make($npassword);
        $user =  User::select("*")
        ->where("id", "=", Session::get('user_id'))->first();
        if($user){
            if (password_verify($cpassword, $user->password)) 
            {
                if($npassword==$confirmpassword) {
                    $post = User::find($user->id);
                    $post->password = $pass;
                    $post->save();

                    if(Auth::guard()->attempt(array('email' => $user->email, 'password' => $npassword)))
                    {
                        // Authentication passed...
                        Session::put('user_id', $user->id);
                        Session::put('user_name', $user->name);
                    }

                    Session::flash('success', 'Password Changed Successfully!'); 
                   
                } else {
                    Session::flash('error', 'Confirm password does not match!'); 
                   
                }
            } else {
                Session::flash('error', 'Current password does not match!'); 
                
            }
        }
    
        return redirect('/my-account');

    }

    public function myAccount() {
        $this->securePage();
        $categories = Category::getCategories();
        $user_id = Session::get('user_id');
        $data = array(
            "categories" => $categories,
            'controller'=>$this,
            "countries" => Country::all(),
            "states" => State::select("*")->where("country_id", "=", 101)->get(),
            "cities" => City::select("*")->where("state_id", "=", 35)->get(),
            'user' => User::find(Session::get('user_id')),
            'orders' => Order::select('*')
            ->where("UserId", "=", $user_id)
            ->orderBy('id', 'DESC')->get()
        );
        return view('site.myaccount')->with($data);
    }
    
    public function viewOrder($id) {
        $this->securePage();
        $categories = Category::getCategories();
        $user_id = Session::get('user_id');
        $data = array(
            "categories" => $categories,
            'controller'=>$this,
            'user' => User::find(Session::get('user_id')),
            'order' => Order::find($id),
            'shipping' => ShippingDetail::find($id),
            'orders' => OrderDetail::select('*')
            ->join('products','products.id','=','order_details.ProductId')
            ->where("order_details.OrderId", "=", $id)
            ->orderBy('order_details.id', 'ASC')->get()
        );
        return view('site.vieworder')->with($data);
    }

    public function convert_number($number) {
            if (($number < 0) || ($number > 999999999)) 
            {
                throw new Exception("Number is out of range");
            }
            $giga = floor($number / 1000000);
            // Millions (giga)
            $number -= $giga * 1000000;
            $kilo = floor($number / 1000);
            // Thousands (kilo)
            $number -= $kilo * 1000;
            $hecto = floor($number / 100);
            // Hundreds (hecto)
            $number -= $hecto * 100;
            $deca = floor($number / 10);
            // Tens (deca)
            $n = $number % 10;
            // Ones
            $result = "";
            if ($giga) 
            {
                $result .= $this->convert_number($giga) .  "Million";
            }
            if ($kilo) 
            {
                $result .= (empty($result) ? "" : " ") .$this->convert_number($kilo) . " Thousand";
            }
            if ($hecto) 
            {
                $result .= (empty($result) ? "" : " ") .$this->convert_number($hecto) . " Hundred";
            }
            $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
            if ($deca || $n) {
                if (!empty($result)) 
                {
                    $result .= " and ";
                }
                if ($deca < 2) 
                {
                    $result .= $ones[$deca * 10 + $n];
                } else {
                    $result .= $tens[$deca];
                    if ($n) 
                    {
                        $result .= "-" . $ones[$n];
                    }
                }
            }
            if (empty($result)) 
            {
                $result = "zero";
            }
            return $result;
    }

    public function getCountryName($id) {
        $country = Country::find($id);
        return $country->name;
    }

    public function getStateName($id) {
        $country = State::find($id);
        return $country->name;
    }

    public function getCityName($id) {
        $country = City::find($id);
        return $country->name;
    }

    public function printInvoice($id) {
        $this->securePage();
        $categories = Category::getCategories();
        $user_id = Session::get('user_id');
        $data = array(
            "categories" => $categories,
            'controller'=>$this,
            'user' => User::find(Session::get('user_id')),
            'order' => Order::find($id),
            'shipping' => ShippingDetail::find($id),
            'orders' => OrderDetail::select('*')
            ->join('products','products.id','=','order_details.ProductId')
            ->where("order_details.OrderId", "=", $id)
            ->orderBy('order_details.id', 'ASC')->get()
        );
        return view('site.printInvoice')->with($data);
    }

    public function securePage() {
        $user_id = Session::get('user_id');
        if($user_id) {
            $user = User::find($user_id);
        } else {
            $user = '';
            Session::flash('error', 'Please login or create account to order our products..!'); 
            return redirect('/login');
        }
    }

    public function renderCart() {
        return view('site.rendercart');
    }

    public function error() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.404')->with($data);
    }

    public function aboutus() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.about')->with($data);
    }
    public function termsAndConditions() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.terms')->with($data);
    }
    public function privacyPolicy() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.privacy')->with($data);
    }
    public function returnPolicy() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.return')->with($data);
    }

    public function contactus() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.contact')->with($data);
    }

    public function success() {
        $categories = Category::getCategories();
        $data = array(
            "categories" => $categories,
            'controller'=>$this
        );
        return view('site.success')->with($data);
    }

    public function logout()
    {
        Session::forget('user_id','user_name','cart','page');
        Session::put('page', '');
        return redirect('/');
    }

    
}
