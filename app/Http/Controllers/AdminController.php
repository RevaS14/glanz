<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Category;
use App\Models\User;
use App\Models\Slider;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ShippingDetail;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use Hash;
use Session;
use Storage;
use Image;

class AdminController extends Controller
{
    public function index() {
        if(Session::get('user_id')){
            return redirect('/admin/dashboard');
        }
        return view('admin.index');
    }

    public function login(Request $request) {
        $request = Request::instance();
        $email = $request->input('email');
        $user =  User::select("*")->where("email", "=", $email)->first();
        if($user){
            if($user->user_type=='A'){
                if (Hash::check($request->input('password'), $user->password)) {
                    Session::put('user_id', $user->id);
                    return redirect('/admin/dashboard');
                } else {
                    Session::flash('error', 'Invalid Credentials!'); 
                }
            } else {
                Session::flash('error', 'Invalid Access!'); 
            }
        } else {
            Session::flash('error', 'User not found!'); 
        }

        return view('admin.index');
    }

    public function dashboard() {
        $this->securePage();
        return view('admin.dashboard');
    }

    public function categories() {
        $this->securePage();
        $data['data'] = Category::getCategories();
        return view('admin.categories')->with($data);
    }

    public function allInvoices() {
        $this->securePage();
        $data['data'] = Invoice::all();
        return view('admin.allinvoice')->with($data);
    }

    public function addCategory() {
        $data['data'] = '';
        return view('admin.categoryInfo')->with($data);
    }

    public function editCategory($id) {
        $data['data'] = Category::find($id);
        return view('admin.categoryInfo')->with($data);
    }

    public function manageCategory(Request $request) {
        $request = Request::instance();
        $id = $request->input('categoryId');

        if($id) {
            $post = Category::find($id);
            $post->CategoryName = $request->input('categoryName');
            $post->CategoryLink = strtolower(str_replace("-"," ",$request->input('categoryName')));
           
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {
            $file = Category::create([
                'CategoryName' => $request->input('categoryName'),
                'CategoryLink' => strtolower(str_replace("-"," ",$request->input('categoryName'))),
                'Status' => $request->input('status')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/admin/categories');
    }

    public function createInvoice(Request $request) {
        $request = Request::instance();
        $post = new Invoice();
        $post->invoice_no = $request->input('invoice_no');
        $post->invoice_for = $request->input('invoice_for');
        $invDate = date('Y-m-d',strtotime($request->input('invoice_date')));
        $post->invoice_date = $invDate;
        $post->bill_to = $request->input('bill_to');
        $post->delivery_note = $request->input('delivery_note');
        $post->buyers_order_no = $request->input('buyers_order_no');
        $post->dispatch = $request->input('dispatch');
        $post->lr = $request->input('lr');
        $post->suppliers_ref = $request->input('suppliers_ref');
        $post->mode = $request->input('mode');
        $Date = date('Y-m-d',strtotime($request->input('date')));
        $post->date = $Date;
        $post->destination = $request->input('destination');
        $post->transport = $request->input('transport');
        $post->others = $request->input('others');
        $post->transport_charges = $request->input('transport_charges');
        $post->cgst = $request->input('cgst_ip');
        $post->sgst = $request->input('sgst_ip');
        $post->gst = $request->input('gst_ip');
        $post->taxable_value = $request->input('taxable_value');
        $post->total = $request->input('gtotal_ip');
        $post->created_at = date('Y-m-d h:i:s');
        $post->save();
        $invoiceId = $post->id;

        $products = $request->input('description');
        $qty = $request->input('qty');
        $price = $request->input('rate');
        $discount = $request->input('discount');
        $numbers = array();
        foreach($discount as $row) {
            $numbers[] = (int)($row);
        }
       var_dump($numbers);
        $i=0; foreach($products as $row) {
            $total = $price[$i] * $qty[$i];
            
            var_dump($numbers[$i]);
            if($numbers[$i]) {
                $discount = $numbers[$i];
            } else {
                $discount = 0;
            }


            
            if($discount) {
            $total = $total - ($total /100) * $discount;
            }
            var_dump($discount);

            $item = new InvoiceItem();
            $item->invoice_id = $invoiceId;
            $item->product = $products[$i];
            $item->qty = $qty[$i];
            $item->price = $price[$i];
            $item->discount = $discount;
            $item->total = $total;
            $item->save();
              
        $i++; }


        Session::flash('success', 'Successfully Created!'); 
        return redirect('/admin/invoice');
    }

    public function delete(Request $request) {
        $request = Request::instance();
        $model = $request->input('table');
        if($model=='Category'){
        $data = Category::find($request->input('id'));
        }
        if($model=='Slider'){
            $data = Slider::find($request->input('id'));
        }
        if($model=='ProductImage'){
            $data = ProductImage::find($request->input('id'));
        }
        if($model=='Product'){
            $data = Product::select("*")->where("id", "=", $request->input('id'))->first();
            $var = ProductCategory::select("*")->where("ProductId", "=", $request->input('id'));
            $var->delete();
            $img = ProductImage::select("*")->where("ProductId", "=", $request->input('id'));
            $img->delete();
        }
        if($model=='Location'){
            $data = Location::find($request->input('id'));
        }
        $data->delete();
    }

    //Banner 

    public function banners() {
        $this->securePage();
        $data['data'] = Slider::all();
        return view('admin.banners')->with($data);
    }

    public function addBanner() {
        $data['data'] = '';
        return view('admin.bannerInfo')->with($data);
    }

    public function editBanner($id) {
        $data['data'] = Slider::find($id);
        return view('admin.bannerInfo')->with($data);
    }

    public function manageBanner(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('sliderId');
        $caption = $request->input('caption');
        $description = $request->input('description');
        $link = $request->input('link');
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $fileName = 'banner'.time();
                $name = $fileName.'.'.$extension;
                $request->file('image')->move(public_path().'/assets/img/slider', $name);  
                $url = 'assets/img/slider/'.$name;
            }
        }
        if($id) {
            $post = Slider::find($id);
            if($url) {
                $post->ImagePath = $url;
            }
            $post->Caption = $caption;
            $post->Description = $description;
            $post->Link = $link;
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {
            $file = Slider::create([
                'ImagePath' => $url,
                'Caption' => $caption,
                'Link' => $link,
                'Description' => $description,
                'Status' => $request->input('status')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/admin/banners');
    }

    //End of Banner


    //Product 

    public function products() {
        $this->securePage();
        $data['data']  = Product::all();
        $data['controller']  = $this;

        return view('admin.products')->with($data);
    }

    public function addProduct() {
        $data['data'] = '';
        $data['categories'] = Category::all();
        return view('admin.productInfo')->with($data);
    }

    public function editProduct($id) {
        $data['data'] = Product::select("*")->where("id", "=", $id)->first();
        $data['categories'] = Category::all();
        $data['controller'] = $this;
        
        $data['images'] = ProductImage::select("*")->where("ProductId", "=", $id)->get();
        return view('admin.productInfo')->with($data);
    }

    public function updateOrder(Request $request) {
        $request = Request::instance();
        $id = $request->input('orderId');
        $status = $request->input('status');
        $order = Order::find($id);
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $fileName = 'LR'.time();
                $name = $fileName.'.'.$extension;
                $request->file('image')->move(public_path().'/assets/img/LR', $name);  
                $url = 'assets/img/LR/'.$name;
                $order->LR = $url;
            }
            $order->DeliveryDate = $request->input('deliveryDate');
        }

        $order->Status = $status;
        $order->save();
        Session::flash('success', 'Successfully Updated!'); 
        return redirect('/admin/view-order/'.$id);
    }
    

    public function manageProduct(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('productId');
        
        if($id) {
            $post = Product::select("*")->where("id", "=", $id)->first();
            Session::flash('success', 'Successfully Updated!'); 
            $productId = $id;
            
        } else {
            $post = new Product;
            Session::flash('success', 'Successfully Created!'); 
        }
        $post->ProductName = $request->input('productName');
        //$post->Category = $request->input('category');
        $link = strtolower(str_replace(" ","-",$request->input('productName')));
        $link = strtolower(str_replace("/","-",$link));
        $link = strtolower(str_replace("(","-",$link));
        $link = strtolower(str_replace(")","-",$link));
        $link = strtolower(str_replace("+","-",$link));
        $post->ProductLink = $link;
        $post->Description = $request->input('description');
        $post->Discount = $request->input('discount');
        $post->Price = $request->input('price');
        $post->Weight = $request->input('weight');
        $post->Stock = $request->input('stock');
        $post->IsFeatured = $request->input('isFeatured');
        $post->Status = $request->input('status');
        $post->save();
       
        if(!$id) {
        $productId = $post->id;
        }

        $category = $request->input('category');

        if($id) {
            ProductCategory::select("*")->where("ProductId", "=", $id)->delete();
        }

        foreach($category as $row)
        {
            $post1 = new ProductCategory;
            $post1->ProductId = $productId;
            $post1->CategoryId = $row;
            $post1->save();
        }

        if ($request->hasFile('image')) {

            foreach($request->file('image') as $file)
            {
                $rnd = rand(0,9999);
                $name = $rnd.'-'.str_replace(' ', '-', $link).'.'.$file->extension();
                
                $path = public_path('/assets/img/s-product');
                $imgx = Image::make($file->getRealPath());
                $imgx->resize(90, 90, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$name);
                
                $path = public_path('/assets/img/product');
                $imgx = Image::make($file->getRealPath());
                $imgx->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$name);
                

                $file->move(public_path().'/assets/img/l-product', $name);  
            
                $url_large = '/assets/img/l-product/'.$name;

                $url_thumb = '/assets/img/s-product/'.$name;

                $url_middle = '/assets/img/product/'.$name;


                ProductImage::create([
                    'Image' => $url_middle,
                    'Thumb' => $url_thumb,
                    'Zoom' => $url_large,
                    'ProductId' => $productId
                ]);

            }
        }

        return redirect('/admin/products');
    }

    //End of Product

    public function getSelected($productId, $categoryId) {
        return ProductCategory::select("*")->where("ProductId", "=", $productId)->where("CategoryId", "=", $categoryId)->first();
    }

    public function getImage($productId) {
        return ProductImage::select("*")->where("ProductId", "=", $productId)->first();
    }

    //users 

    public function users() {
        $this->securePage();
        $data['data']  = User::all();
        return view('admin.users')->with($data);
    }

    // End of users

    public function getTotalItems($id) {
        $items= OrderDetail::select("*")
        ->where('OrderId','=',$id)
        ->get();
        echo count($items);
    }

    public function completeOrder($email, $id) {
        $order = Order::find($id);
        $order->Status = 'C';
        $order->save();

        $this->sendMail($email,$id);

        Session::flash('success', 'Order has been completed successfully..!');
        return redirect('/view-order/'.$id);
    }

    //orders 

    public function orders() {
        $this->securePage();
        $data['controller'] = $this;
        $data['data']  = Order::select("orders.*","users.name")
        ->join('users','users.id','=','orders.UserId')
        ->orderBy('id','DESC')
        ->get();
        return view('admin.orders')->with($data);
    }

    // End of orders

    //location

    public function locations() {
        $this->securePage();
        $data['data'] = Location::all();
        return view('admin.locations')->with($data);
    }

    public function addLocation() {
        $data['data'] = '';
        return view('admin.locationInfo')->with($data);
    }

    public function editLocation($id) {
        $data['data'] = Location::find($id);
        return view('admin.locationInfo')->with($data);
    }

    public function manageLocation(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('locationId');

       

        if($id) {
            $post = Location::find($id);
            $post->name = $request->input('name');
            $post->pincode = $request->input('pincode');
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {

            $exists = Location::where('name', $request->input('name'))->first();
            if($exists) {
                Session::flash('error', 'Location Name Already Exists..!');
                return redirect('/add-location');
            }
    
            $pincode_exists = Location::where('pincode', $request->input('pincode'))->first();
            if($pincode_exists) {
                Session::flash('error', 'Pincode Already Exists..!');
                return redirect('/add-location');
            }

            $file = Location::create([
                'name' => $request->input('name'),
                'pincode' => $request->input('pincode'),
                'status' => $request->input('status'),
                'created_at' => date('Y-m-d h:i:s a')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/locations');
    }

    //end of location

    //orders detail
    
    public function getProductSingleImage($id) {
        $singleImage =  ProductImage::getProductSingleImage($id);
        return $singleImage->Image;
    }

    public function getSingleProduct($id){
        return Product::getSingleProduct($id);
    }

    
    public function readableMetric($mass)
    {
        $units = [
            -3 => "Tonne",
             0 => "Kg",
             3 => "Grams",
             6 => "Mg",
        ];
    
        foreach ($units as $x => $unit) {
            if ( ($newMass = $mass * 10 ** $x)  >= 1 ) {
                return "{$newMass} {$unit}";
            }
        }
    }

    public function viewOrder($id) {
        $this->securePage();
        $data['order'] = $order = Order::find($id);
        $data['customer']  = User::find($order->UserId);
        $data['controller'] = $this;
        $data['shipping']  = ShippingDetail::where('id', $order->ShippingId)->first();
        $data['orders']  = OrderDetail::select('*')
        ->join('products','products.id','=','order_details.ProductId')
        ->where("order_details.OrderId", "=", $id)
        ->orderBy('order_details.id', 'ASC')->get();
        //->groupBy('product_images.ProductId')
   
        return view('admin.vieworder')->with($data);
    }

    public function printInvoice($id) {
        $this->securePage();
        $data['order'] = $order = Order::find($id);
        $data['customer']  = User::find($order->UserId);
        $data['shipping']  = ShippingDetail::where('id', $order->ShippingId)->first();
        $data['orders']  = OrderDetail::select("*")
        ->join('products','products.id','=','order_details.ProductId')
        ->join('product_images','product_images.ProductId','=','order_details.ProductId')
        ->where('order_details.OrderId','=',$id)
        //->groupBy('product_images.ProductId')
        ->get();
        return view('admin.printInvoice')->with($data);
    }

    public function printMinvoice($id) {
        $this->securePage();
        $data['order'] = $order = Invoice::find($id);
        $data['controller'] = $this;
        $data['orders']  = InvoiceItem::select("*")
        ->where('invoice_id','=',$id)
        //->groupBy('product_images.ProductId')
        ->get();
        return view('admin.printMInvoice')->with($data);
    }

    public function convert_number($number) {
        if (($number < 0) || ($number > 999999999)) 
        {
            throw new Exception("Number is out of range");
        }
        $giga = floor($number / 1000000);
        // Millions (giga)
        $number -= $giga * 1000000;
        $kilo = floor($number / 1000);
        // Thousands (kilo)
        $number -= $kilo * 1000;
        $hecto = floor($number / 100);
        // Hundreds (hecto)
        $number -= $hecto * 100;
        $deca = floor($number / 10);
        // Tens (deca)
        $n = $number % 10;
        // Ones
        $result = "";
        if ($giga) 
        {
            $result .= $this->convert_number($giga) .  "Million";
        }
        if ($kilo) 
        {
            $result .= (empty($result) ? "" : " ") .$this->convert_number($kilo) . " Thousand";
        }
        if ($hecto) 
        {
            $result .= (empty($result) ? "" : " ") .$this->convert_number($hecto) . " Hundred";
        }
        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
        if ($deca || $n) {
            if (!empty($result)) 
            {
                $result .= " and ";
            }
            if ($deca < 2) 
            {
                $result .= $ones[$deca * 10 + $n];
            } else {
                $result .= $tens[$deca];
                if ($n) 
                {
                    $result .= "-" . $ones[$n];
                }
            }
        }
        if (empty($result)) 
        {
            $result = "zero";
        }
        return $result;
}

    public function viewUser($id) {
        $this->securePage();
        $data['user']  = User::find($id);
        $data['controller'] = $this;
        $data['orders']  = Order::where('UserId', $id)->orderBy('OrderId','DESC')->get();
        return view('admin.view-user')->with($data);
    }

    public function sendMail($email,$order_id) {
        
        $items = OrderDetail::select("*")
        ->join('products','order_details.ProductId','=','products.ProductId')
        ->where("OrderId", "=", $order_id)->get();

        $order = Order::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $shipping = ShippingDetail::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $data = array('email'=>$email,'items'=>$items,'order'=>$order,'shipping'=>$shipping);
        Mail::send('mail.order', $data, function($message) use ($email) {
           $message->to($email)->subject
              ('Your Order has been delivered from Glanz Special Tools');
           $message->from('info@thathafishshop.com','Glanz Special Tools');
        });
        //echo "HTML Email Sent. Check your inbox.";
     }

    // End of orders detail

    public function invoice() {
        $this->securePage();
        $data['data'] = '';
        return view('admin.invoice')->with($data);
    }

    public function addOrder() {
        $data['data'] = '';
        return view('admin.orderInfo')->with($data);
    }

    public function securePage() {
        if(!Session::get('user_id')){
            return redirect('/');
        }
    }
}
