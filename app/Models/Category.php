<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $fillable = ['CategoryName','CategoryLink','Status'];
    use HasFactory;

    public static function getCategories(){
        $value=DB::table('categories')->orderBy('id', 'desc')->get();
        return $value;
    }

    public static function getProducts($id){
        $value=DB::table('product_categories')
        ->join('products','products.id','=','product_categories.ProductId')
        ->where("product_categories.CategoryId",$id)->orderBy('product_categories.id', 'desc')->simplePaginate(20);
        return $value;
    }

    public static function getCategoryByLink($link){
        $value=DB::table('categories')->where("CategoryLink",$link)->first();
        return $value;
    }

    public static function getProductCategory($id){
        $value=DB::table('product_categories')
        ->join('categories','categories.id','=','product_categories.CategoryId')
        ->where("ProductId",$id)
        ->first();
        return $value;
    }
}
