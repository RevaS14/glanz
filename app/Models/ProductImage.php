<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductImage extends Model
{
    use HasFactory;
    protected $fillable = ['Image','Thumb','Zoom','ProductId'];

    public static function getProductSingleImage($id){
        $value=DB::table('product_images')->where('ProductId',$id)->orderBy('id', 'desc')->first();
        return $value;
    }

    public static function getProductImages($id){
        $value=DB::table('product_images')->where('ProductId',$id)->orderBy('id', 'desc')->get();
        return $value;
    }
}
