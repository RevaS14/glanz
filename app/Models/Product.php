<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    use HasFactory;

    public static function getFeaturedProducts(){
        $value=DB::table('products')->where('IsFeatured','Y')->orderBy('id', 'desc')->get();
        return $value;
    }

    public static function getRelatedProducts($id){
        $value=DB::table('product_categories')
        ->join('products','products.id','=','product_categories.ProductId')
        ->where('product_categories.CategoryId',$id)->orderBy('product_categories.id', 'desc')->get();
        return $value;
    }

    public static function getProductByLink($link){
        $value=DB::table('products')->where("ProductLink",$link)->first();
        return $value;
    }

    public static function searchProducts($keyword){
        $value=DB::table('products')->where("ProductName",'like', '%' .$keyword. '%')->get();
        return $value;
    }
    

    public static function getSingleProduct($id){
        $value=DB::table('products')->where('id',$id)->first();
        return $value;
    }

    public static function getofferedProducts(){
        $value=DB::table('products')->where('Discount','!=',0)->orderBy('id', 'desc')->get();
        return $value;
    }

    public static function getmostViewdProducts(){
        $value=DB::table('products')->orderBy('ViewCount', 'desc')->get();
        return $value;
    }
}
