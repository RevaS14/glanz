<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = ['Caption','Description','ImagePath','Link','Status'];

    public static function getSliders(){
        $value=DB::table('sliders')->orderBy('id', 'asc')->get();
        return $value;
    }
}
